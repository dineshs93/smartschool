<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Exam_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	/*
	 * Get the exam details based on exam id its come from [Exam-controller]-> questions() function
	 *
	 */
	public function exam_detail($exam_id) {
		$query = $this->db->query ( "select * from
				exam where id=$exam_id 
				" );
		return $query->row ();
	}
	
	/*
	 * Get the Question details
	 *
	 */
	public function question_detail($exam_id) {
		$query = $this->db->query ( "select * from question where exam_id=$exam_id" );
		
		return $query->result_array ();
	}
	
	/*
	 * Question edit opton for modaal details
	 */
	public function question_edit($question_id) {
		$query = $this->db->query ( "select * from question where id=$question_id" );
		return $query->row ();
	}
	
	/*
	 * function return the exam list details
	 */
	public function exam_list() {
		$query = $this->db->query ( "select * ,(SELECT GROUP_CONCAT( name ) FROM class WHERE FIND_IN_SET( id, exam.class )) AS class_name from exam" );
		
		return $query->result_array ();
	}
	
	/*
	 * function return the exam edit opton
	 */
	public function exam_edit($exam_id) {
		$query = $this->db->query ( "select * from exam where id=$exam_id" );
		
		return $query->row ();
	}
	
	/*
	 * GEt assigned mark based on exam
	 */
	public function assigned_mark($exam_id) {
		$query = $this->db->query ( "select sum(mark) as mark from question where exam_id=$exam_id" );
		return $query->row ()->mark;
	}
	
	/*
	 * Class list function
	 */
	public function clas() {
		$query = $this->db->query ( "select * from class" );
		
		return $query->result_array ();
	}
}