<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Studentexam_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	/*
	 * list out the exam list based on student class and exam status
	 */
	public function stdexam_list($std_id) {
		$query = $this->db->query ( "select * from exam where class=(select class from student where id=$std_id) AND id NOT IN (select exam_id from exam_details where std_id=$std_id AND status=1) " );
		return $query->result_array ();
	}
	
	/*
	 * Instructions about exam
	 */
	public function instructions($exam_id) {
		$query = $this->db->query ( "select *,(select count(id) from question where exam_id IN ($exam_id)) as question_count from exam where id=$exam_id" );
		return $query->row ();
	}
	
	/*
	 * Exam details for student exam page
	 */
	public function exam_details($exam_id) {
		$query = $this->db->query ( "select * from exam where id=$exam_id" );
		return $query->row ();
	}
	/*
	 * Questions details for student exam page
	 */
	public function question_details($exam_id) {
		$query = $this->db->query ( "select * from question where exam_id=$exam_id" );
		return $query->result_array ();
	}
	/*
	 * check exam detals already completed or not by student
	 */
	public function exam_check($exam_id, $std_id) {
		$query = $this->db->query ( "select * from exam_details where std_id=$std_id AND exam_id=$exam_id" );
		return $query->num_rows ();
	}
	
	/*
	 * Check exam status for redirect exam page if already completed
	 */
	public function status_check($exam_id, $std_id) {
		$query = $this->db->query ( "select * from exam_details where std_id=$std_id AND exam_id=$exam_id" );
		
		if($query->num_rows ()>0){
			return $query->row ()->status;
		}else{
			return  0;
		}
	}
	
	/*
	 * 
	 */
	
	public function examsummary_details($exam_id){
		$query = $this->db->query ( "select *,(select name from exam where id=$exam_id)as exam_name from exam_details where exam_id=$exam_id" );
		return $query->row ();
		
	}
	
	/*
	 * Update the exam details with obtained mark ,exam status
	 */
	public function exam_update($exam_id, $std_id) {
		
		$query = $this->db->query ("SELECT SUM( question.mark ) as total_mark,(select pass_mark from exam where id=$exam_id) as pass_mark
									FROM question_details
									INNER JOIN question ON question_details.question_id = question.id
									WHERE question_details.exam_id =$exam_id AND question_details.std_id=$std_id 
									AND ( CASE WHEN question.type=1 THEN
									question_details.answer = question.answer
				WHEN question.type=2 THEN question_details.answer =question.option_ans
				END
										)				
				" );
		$toatl_mark=$query->row()->total_mark;
		$pass_mark=$query->row()->pass_mark;
		
		if($toatl_mark >= $pass_mark){
			$pass_status=1;
		}else{
			$pass_status=0;
		}
		
		$data=array(
				'end_time'=>date('Y-m-d H:i:s'),
				'status'=>1,
				'marks_obtained'=>$toatl_mark,
				'pass_status'=>$pass_status
				
		);
		
		$this->db->where('std_id',$std_id);
		$this->db->where('exam_id',$exam_id);
		$this->db->update('exam_details',$data);
		
		return true;
		
	}
	
	/*
	 * Question details with correct answer and wrong answer 
	 * 
	 */
	
	public function questions_result($exam_id,$std_id){
		
		$query=$this->db->query("select question.question as question, question.mark as mark ,(CASE WHEN question.type=1 THEN question_details.answer ELSE (CASE WHEN question_details.answer='optiona' THEN question.optiona WHEN question_details.answer='optionb' THEN question.optionb WHEN question_details.answer='optionc' THEN question.optionc WHEN question_details.answer='optiond' THEN question.optiond END) END) as your_answer , (CASE WHEN question.type=1 THEN question.answer ELSE (CASE WHEN question.option_ans='optiona' THEN question.optiona WHEN question.option_ans='optionb' THEN question.optionb WHEN question.option_ans='optionc' THEN question.optionc WHEN question.option_ans='optiond' THEN question.optiond END) END) as correct_answer from question_details INNER JOIN question on question_details.question_id=question.id where question_details.exam_id=$exam_id AND question_details.std_id=$std_id");
		return $query->result_array();
	}
	
	
	/*
	 * Completed exam List
	 */
     public function completed_exam($std_id){
     	
     	$query=$this->db->query("select exam.id as exam_id,exam.name as exam_name,exam_details.total_mark as total_mark,exam_details.pass_mark as pass_mark,exam_details.pass_status as pass_status,exam_details.marks_obtained as marks_obtained from exam_details INNER JOIN exam on exam_details.exam_id=exam.id where exam_details.std_id=$std_id and exam_details.status=1");
     
        return $query->result_array(); 
     }
	
	/*
	 * Count the exam details based on students 
	 */
	
     public function count_details($std_id){
     	$query=$this->db->query("select count(id) as id from exam where class IN (select class from student  where id=$std_id) AND id NOT IN (select exam_id from exam_details where std_id=$std_id AND status=1)");
     	return $query->row();
     }
	
	
	
}