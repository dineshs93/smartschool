<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model{
	function __construct(){
		parent::__construct();
		

	}
	
	public function login($username,$password,$role){
		
		$this->db->where('username', $username);
		$this->db->where('password',md5($password));
		if($role ==1){
		
		$query = $this->db->get('teacher');
		}elseif($role ==2){
		$query = $this->db->get('student');
		}
		$result=$query->num_rows();
		if($result == 1){
		
			$row = $query->row();
			$data = array(
					'id'=>$row->id,
					'username' => $row->username,
					'role'=>$role,
					'validated' => true
			);
			$this->session->set_userdata($data);
			return true;
		
		}
	}
	
	/*
	 * count the teachers,student,exams
	 */
	
	public function count_details(){
		
		$query=$this->db->query("select count(id) as teacher,(select count(id) from student) as student ,(select count(id) from exam) as exam    from teacher");
	    return $query->row();
	}
	
	/* 
	 * profile page get details based on user id and role 
	 */
	
	public function user_detail($id,$role){
		
		if($role ==1){
			$query=$this->db->query("select * from teacher where id=$id");
			$result= $query->row();
			
		}else{
			$query=$this->db->query("select * from student where id=$id");
			$result= $query->row();
		}
		
		return $result;
	}
}