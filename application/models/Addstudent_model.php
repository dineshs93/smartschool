<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addstudent_model extends CI_Model{
	function __construct(){
		parent::__construct();


	}
	public  function studentlist(){

		$query = $this->db->query('SELECT * FROM student');
		return $query->result();
	}//end of student list in table function
	public function studentlist_edit($id)
	{

		$query=$this->db->query("SELECT student.id,student.name, student.age,student.class, class.name c_name, class.id c_id FROM student INNER JOIN class ON student.class=class.id WHERE student.id=$id");
		return $query->result();


	}//end of student list for select box edit function
	public function clas()
	{
	
		$query=$this->db->query("select * from class");
		return $query->result();
	
	
	}//end of student  list for select box function
}
?>
