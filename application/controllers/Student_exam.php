<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Student_exam extends CI_Controller {
	
	
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url','html','date'));
		$this->load->library(array('form_validation','session'));
		$this->load->model('studentexam_model');
	}
	
	/*
	 * Load the exam list for students 
	 */
	
	public function index(){
		
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		
		$data['exam_list']=$this->studentexam_model->stdexam_list($this->session->userdata('id'));
		
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('studentexam_list',$data);
			$this->load->view('footer',$data);
		}
		
	}
	
	/*
	 * Instructions for exam 
	 */
	
	public function exam_instructions(){
		
		$exam_id=$this->input->post('id');
		$data['exam_instructions']=$this->studentexam_model->instructions($exam_id);
		
		$this->load->view('exam_instructions',$data); 
		
		
	}
	
	/*
	 * Exam page display the questions and student can answer the questions 
	 */
	
	public function exam(){
		$exam_id=$this->uri->segment(3);
        $status_check=0;
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		
		$exam_details=$this->studentexam_model->exam_details($exam_id);
		$exam_check=$this->studentexam_model->exam_check($exam_id,$this->session->userdata('id'));
		$status_check=$this->studentexam_model->status_check($exam_id,$this->session->userdata('id'));
		if($status_check ==1){
			redirect('student_exam');
		}
		if($exam_check > 0){
			$data=array('start_time'=>date('Y-m-d H:i:s'),'total_mark'=>$exam_details->total_mark,'pass_mark'=>$exam_details->pass_mark);
			$this->db->where('std_id',$this->session->userdata('id'));
			$this->db->where('exam_id',$exam_id);
			$this->db->update('exam_details',$data);
			
			
		}else{
		$data=array(
				'std_id'=>$this->session->userdata('id'),
				'exam_id'=>$exam_id,
				'start_time'=>date('Y-m-d H:i:s'),
				'total_mark'=>$exam_details->total_mark,
				'pass_mark'=>$exam_details->pass_mark
				
		);
		$this->db->insert('exam_details',$data);
		}
		
		$data['exam_details']=$this->studentexam_model->exam_details($exam_id);
		$data['question_details']=$this->studentexam_model->question_details($exam_id);
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('exam_page',$data);
			$this->load->view('footer',$data);
		}
		
	}
	
	/*#from student exam page 
	 * Answer insertion based on exam and students
	 */
	
	public function answer_insertion(){
		
		$exam_id=$this->input->post('exam_id');
		$std_id=$this->input->post('std_id');
		$question_id=$this->input->post('question_id');
		$answer=$this->input->post('answer');
		$data=array(
				
				'exam_id'=>$exam_id,
				'question_id'=>$question_id,
				'std_id'=>$std_id,
				'answer'=>$answer
				
		);
		$this->db->insert('question_details',$data);
		return 1;
		
	}
	
	/*
	 * Result and summary page after submit exam
	 * 
	 */
	
	public function exam_summary(){
		
		$exam_id=$this->uri->segment(3);
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		
		$data['exam_update']=$this->studentexam_model->exam_update($exam_id,$this->session->userdata('id'));
		$data['exam_details']=$this->studentexam_model->examsummary_details($exam_id);
		$data['question_details']=$this->studentexam_model->questions_result($exam_id,$this->session->userdata('id'));
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('exam_summary',$data);
			$this->load->view('footer',$data);
			
		}
		
		
	}
	
	/*
	 * Completed Exam List 
	 */
	
	public function exam_list(){
		
		
		$data['exam_list']=$this->studentexam_model->completed_exam($this->session->userdata('id'));
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('completed_exam',$data);
			$this->load->view('footer',$data);
				
		}
		
	}
	
	/*
	 * Exam result page 
	 */
	public function exam_result(){
		
		$exam_id=$this->uri->segment(3);
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		
		
		$data['exam_details']=$this->studentexam_model->examsummary_details($exam_id);
		$data['question_details']=$this->studentexam_model->questions_result($exam_id,$this->session->userdata('id'));
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('exam_result',$data);
			$this->load->view('footer',$data);
				
		}
		
		
	}
}
	