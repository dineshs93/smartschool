<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
header ( 'Access-Control-Allow-Origin: *' );
class Login extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( array (
				'form',
				'url',
				'html',
				'date' 
		) );
		$this->load->library ( array (
				'form_validation',
				'session' 
		) );
		$this->load->model ( 'login_model' );
		$this->load->model ( 'studentexam_model' );
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * http://example.com/index.php/welcome
	 * - or -
	 * http://example.com/index.php/welcome/index
	 * - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * 
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		if (! $this->session->userdata ( 'validated' )) {
			$this->load->view ( 'login' );
		} else {
			$data ['username'] = $this->session->userdata ( 'username' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['count_details'] = $this->login_model->count_details ();
			$data ['exam_count'] = $this->studentexam_model->count_details ( $this->session->userdata ( 'id' ) );
			$this->load->view ( 'head', $data );
			$this->load->view ( 'home', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/* Login check process */
	public function login_check() {
		$role = $this->input->post ( 'role' );
		$username = $this->input->post ( 'username' );
		$password = $this->input->post ( 'password' );
		$count = $this->login_model->login ( $username, $password, $role );
		
		if (! $count) {
			echo "failure";
		} else {
			echo "success";
			$this->db->where ( 'username', $username );
			$time = (round ( microtime ( true ) * 1000 ));
			$data = array (
					'last_login' => $time 
			);
			if ($role == 1) {
				$this->db->update ( 'teacher', $data );
			} elseif ($role == 2) {
				$this->db->update ( 'student', $data );
			}
		}
	}
	
	/* Logout function */
	public function do_logout() {
		$this->session->sess_destroy ();
		redirect ( 'login' );
	}
	
	/*
	 * Profile page
	 */
	public function profile() {
		$data ['username'] = $this->session->userdata ( 'username' );
		$data ['id'] = $this->session->userdata ( 'id' );
		$data ['user_detail'] = $this->login_model->user_detail ( $this->session->userdata ( 'id' ), $this->session->userdata ( 'role' ) );
		
		if (! $this->session->userdata ( 'username' )) {
			redirect ( 'login' );
		} else {
			$this->load->view ( 'head', $data );
			$this->load->view ( 'profile', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/*
	 * Teacher update from profile page
	 */
	
	public function teacher_update(){
		
		$name = $this->input->post ( 'name' );
		$email = $this->input->post ( 'email' );
		$uname = $this->input->post ( 'uname' );
		$mob_num = $this->input->post ( 'mob_num' );
		$user_id = $this->input->post ( 'user_id' );
		
		$data=array(
				'name'=>$name,
				'email'=>$email,
				'username'=>$uname,
				'mobile_Number'=>$mob_num
			
		);
		
		$this->db->where('id',$user_id);
		$this->db->update('teacher',$data);
		echo 1;
		
		
		
		
		
	}
	
	/*
	 * Teacher update from profile page
	 */
	
	public function student_update(){
	
		$name = $this->input->post ( 'name' );
		$email = $this->input->post ( 'email' );
		$uname = $this->input->post ( 'uname' );
		$mob_num = $this->input->post ( 'mob_num' );
		$user_id = $this->input->post ( 'user_id' );
	
		$data=array(
				'name'=>$name,
				'mail'=>$email,
				'username'=>$uname,
				'mobile_number'=>$mob_num
					
		);
	
		$this->db->where('id',$user_id);
		$this->db->update('student',$data);
		echo 1;
	
	
	
	
	
	}
	
	/*
	 * Update the student password 
	 */
	public function student_passupdate(){
		
		$data=array(
			'password'=>md5($this->input->post('password'))	
				
		);
		$this->db->where('id',$this->session->userdata('id'));
		$this->db->update('student',$data);
		echo 1;
	}
	
	/*
	 * Update the student password
	 */
	public function teacher_passupdate(){
	
		$data=array(
				'password'=>md5($this->input->post('password'))
	
		);
		$this->db->where('id',$this->session->userdata('id'));
		$this->db->update('teacher',$data);
		echo 1;
	}
}
