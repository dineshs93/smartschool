<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');


class Exam extends CI_Controller {
	
	
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url','html','date'));
		$this->load->library(array('form_validation','session'));
		$this->load->library('doctrine');
		$this->load->model('exam_model');
		
		
	}
	
/* Load the create exam page 
 * 
 */
	
	public function  index(){
	
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$data['clas']	=$this->exam_model->clas();
		
		//$user = new Entity\Test;
		//$user->setTest('dsd');
		//$user->setPassword('Passw0rd');
		//$user->setEmail('wildlyinaccurate@gmail.com');
		//$user->setGroup($group);
		
		// When you have set up your database, you can persist these entities:
		////$em = $this->doctrine->em;
		// $em->persist($group);
		//$em->persist($user);
		//$em->flush();
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
		$this->load->view('head',$data);
		$this->load->view('create_exam',$data);
		$this->load->view('footer',$data);
		}
	}
	
	
	/* Insert exam details into database
	 *
	 */
	
	public function exam_insert(){
		
		$exam_name=$this->input->post('exam_name');
		$date_value=$this->input->post('date');
		$duration=$this->input->post('duration');
		$clas=$this->input->post('clas');
		$description=$this->input->post('description');
		$total_mark=$this->input->post('total_mark');
		$pass_mark=$this->input->post('pass_mark');
		
		$date=explode("-", $date_value);
		$from_date=strtotime($date[0]);
		$to_date=strtotime($date[1]);
		$f_date=date('Y-m-d',$from_date);
		$t_date=date('Y-m-d',$to_date);
		$data=array(
				'name'=>$exam_name,
				'duration'=>$duration,
				'class'=>$clas,
				'start_date'=>$f_date,
				'end_date'=>$t_date,
				'description'=>$description,
				'total_mark'=>$total_mark,
				'pass_mark'=>$pass_mark,
				'created_by'=>$this->session->userdata('id')
				
		);
		
		$this->db->insert('exam',$data);
		$exam_id=$this->db->insert_id();
		echo $exam_id;
		
		
	}
	
	/*
	 * Questions page and manage page 
	 * 
	 */
	
	public function questions(){
		
		$exam_id=$this->uri->segment(3);
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		
		$data['exam_detail']=$this->exam_model->exam_detail($exam_id);
		$data['question_detail']=$this->exam_model->question_detail($exam_id);
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
		$this->load->view('head',$data);
		$this->load->view('questions',$data);
		$this->load->view('footer',$data);
		}
		
	}
	
	
	/*
	 * Insert questions into databse 
	 */
	
	public function question_insert(){
		
		$content=utf8_encode($this->input->post('content'));
		$mark=$this->input->post('mark');
		$ans_type=$this->input->post('ans_type');
		if($ans_type ==1){
		$text_ans=$this->input->post('text_ans');
		$correct_answer="";
		$optiona="";
		$optionb="";
		$optionc="";
		$optiond="";
		}elseif ($ans_type==2){
			
		$text_ans="";
		$correct_answer=$this->input->post('correct_answer');
		$optiona=$this->input->post('optiona');
		$optionb=$this->input->post('optionb');
		$optionc=$this->input->post('optionc');
		$optiond=$this->input->post('optiond');
		}
		$exam_id=$this->input->post('exam_id');
		
		$data=array(
				
				'exam_id'=>$exam_id,
				'question'=>$content,
				'mark'=>$mark,
				'type'=>$ans_type,
				'answer'=>$text_ans,
				'optiona'=>$optiona,
				'optionb'=>$optionb,
				'optionc'=>$optionc,
				'optiond'=>$optiond,
				'option_ans'=>$correct_answer
				
		);
		$this->db->insert('question',$data);
		
		$assigned_mark=$this->exam_model->assigned_mark($exam_id);
		
		$this->db->where('id',$exam_id);
		$data1=array(
				'assigned_mark'=>$assigned_mark
				
		);
		
		$this->db->update('exam',$data1);
		echo 1;
	}
	
	/*
	 * Edit the question and display the modal with value 
	 */
	
	public function question_edit(){
		
		$question_id=$this->input->post("id");
		$data["question_edit"]=$this->exam_model->question_edit($question_id);
		$this->load->view('question_edit',$data);
		
		
	}
	
	/*
	 * Update the question details 
	 * 
	 */
	
	public function  question_update(){
		
		$edit_content=utf8_encode($this->input->post('edit_content'));
		$edit_mark=$this->input->post('edit_mark');
		$edit_ans_type=$this->input->post('edit_ans_type');
		$exam_id=$this->input->post('edit_exam_id');
		if($edit_ans_type ==1){
			$edit_text_ans=$this->input->post('edit_text_ans');
			$edit_correct_answer="";
			$edit_optiona="";
			$edit_optionb="";
			$edit_optionc="";
			$edit_optiond="";
		}elseif ($edit_ans_type==2){
				
			$edit_text_ans="";
			$edit_correct_answer=$this->input->post('edit_correct_answer');
			$edit_optiona=$this->input->post('edit_optiona');
			$edit_optionb=$this->input->post('edit_optionb');
			$edit_optionc=$this->input->post('edit_optionc');
			$edit_optiond=$this->input->post('edit_optiond');
		}
		$edit_question_id=$this->input->post('edit_question_id');
		
		$data=array(
		
				
				'question'=>$edit_content,
				'mark'=>$edit_mark,
				'type'=>$edit_ans_type,
				'answer'=>$edit_text_ans,
				'optiona'=>$edit_optiona,
				'optionb'=>$edit_optionb,
				'optionc'=>$edit_optionc,
				'optiond'=>$edit_optiond,
				'option_ans'=>$edit_correct_answer
		
		);
		
		$this->db->where('id', $edit_question_id);
		$this->db->update('question', $data);
		
		$assigned_mark=$this->exam_model->assigned_mark($exam_id);
		
		$this->db->where('id',$exam_id);
		$data1=array(
				'assigned_mark'=>$assigned_mark
		
		);
		
		$this->db->update('exam',$data1);
		echo 1;
		
		
	}
	
	/*
	 * Question Delete 
	 */
	
	public function question_delete(){
		
		$question_id=$this->input->post('question_id');
		$this->db->where('id',$question_id);
		$this->db->delete('question');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	}
	
	/*
	 * View of exam page list  
	 *
	 */
	
	public function exam_list(){
		
		
		$data['exam_list']=$this->exam_model->exam_list();
		
		if(!$this->session->userdata('username')){
			redirect('login');
		}else{
			$this->load->view('head',$data);
			$this->load->view('exam_list',$data);
			$this->load->view('footer',$data);
		}
	}
	
	/*
	 * Exam delete function 
	 */
	
	public function exam_delete(){
		$exam_id=$this->input->post('exam_id');
		$this->db->where('id',$exam_id);
		$this->db->delete('exam');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
		
	}
	
	/*
	 * Exam edit show the modal with exam details 
	 */
	public function exam_edit(){
		
		$exam_id=$this->input->post("id");
		$data['clas']	=$this->exam_model->clas();
		$data["exam_edit"]=$this->exam_model->exam_edit($exam_id);
		$this->load->view('exam_edit',$data);
		
	}
	
	/*
	 * exam update function 
	 */
	public function exam_update(){
		
		$edit_exam_name=$this->input->post('edit_exam_name');
		$edit_date_value=$this->input->post('edit_date');
		$edit_duration=$this->input->post('edit_duration');
		$edit_clas=$this->input->post('edit_clas');
		$edit_description=$this->input->post('edit_description');
		$edit_total_mark=$this->input->post('edit_total_mark');
		$edit_pass_mark=$this->input->post('edit_pass_mark');
		
		$edit_date=explode("-", $edit_date_value);
		$edit_from_date=strtotime($edit_date[0]);
		$edit_to_date=strtotime($edit_date[1]);
		$edit_f_date=date('Y-m-d',$edit_from_date);
		$edit_t_date=date('Y-m-d',$edit_to_date);
		$data=array(
				'name'=>$edit_exam_name,
				'duration'=>$edit_duration,
				'class'=>$edit_clas,
				'start_date'=>$edit_f_date,
				'end_date'=>$edit_t_date,
				'description'=>$edit_description,
				'total_mark'=>$edit_total_mark,
				'pass_mark'=>$edit_pass_mark,
				'created_by'=>$this->session->userdata('id')
		
		);
		$edit_exam_id=$this->input->post('exam_id');
		$this->db->where('id', $edit_exam_id);
		$this->db->update('exam', $data);
		echo 1;
		
	}
}
	
	?>