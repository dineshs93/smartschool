<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Student extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url','html','date'));
		$this->load->library(array('form_validation','session'));
		$this->load->model('addstudent_model');

	}

	/* Load the add student  page
	 *
	 */


	public function  index(){


		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$data['student']=$this->addstudent_model->clas();
		$this->load->view('head',$data);
		$this->load->view('addstudent',$data);
		$this->load->view('footer',$data);
	}
	
	/* student insert function
	 *
	 */
	
	
		public function  insert(){

		$name=$this->input->post('name');
		$age=$this->input->post('age');
		$uname=$this->input->post('uname');
		$pass=md5($this->input->post('password'));
		$class=$this->input->post('class');
		
		

		$data=array(

				'name'=>$name,
				'age'=>$age,
				'username'=>$uname,
				'password'=>$pass,
				'class'=>$class
				
		);



		$this->db->insert('student',$data);
		$exam_id=$this->db->insert_id();
		echo $exam_id;
			


	}
	

	/* retrive the student list
	 *
	 */
	
	public function  getlist(){
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$data['teacher_list']=$this->addstudent_model->studentlist();
		$this->load->view('head',$data);
		$this->load->view('studentlist',$data);
		$this->load->view('footer',$data);

	}

	/* delete the student list
	 *
	 */
	public function  delete(){
		$user_id=$this->input->post('id');

		$this->db->where('student_id',$user_id);
		$this->db->delete('student');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}

	}

	/* edit the student list
	 *
	 */
	
	public function edit()
	{
		$id=$this->input->post('id');
		$data['result']=$this->addstudent_model->studentlist_edit($id);
		$data['class_list']=$this->addstudent_model->clas();
		$this->load->view('ajax_edit_student',$data);
	}
	

	/* update the student
	 *
	 */
	public function update()
	{

		$rev_id=$this->input->post('rev_id');
		$name=$this->input->post('name');
		$age=$this->input->post('age');
		$uname=$this->input->post('uname');
		$pass=md5($this->input->post('password'));
		$class=$this->input->post('clas');
		
			
		$data=array(
				'id'=>$rev_id,
				'name'=>$name,
				'age'=>$age,
				'username'=>$uname,
				'password'=>$pass,
				'class'=>$class,
				'section'=>$section,
				'cpassword'=>$cpass
		);
			
		$this->db->where('id', $rev_id);
		$this->db->update('student', $data);

		echo($rev_id);
		
	} 
}

?>