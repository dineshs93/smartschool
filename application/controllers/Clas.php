<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Clas extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url','html','date'));
		$this->load->library(array('form_validation','session'));
		$this->load->model('addteacher_model');
		$this->load->model('class_model');

	}

	/* Load the Add class page
	 *
	 */


	public function  index(){
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$data['teacher_list']=$this->addteacher_model->teacherlist();
		$data['clas_list']=$this->class_model->claslist();
		
		$this->load->view('head',$data);
		$this->load->view('add_class',$data);
		$this->load->view('footer',$data);
	}
	/*class insert function
	 *
	 */
	
	
	public function  insert(){
	
		$clas_name=$this->input->post('name');
		$teacher=$this->input->post('teacher');
		$description=$this->input->post('description');
		$data=array(
				'name'=>$clas_name,
				'class_teacher'=>$teacher,
				'descriptin'=>$description
		);
		$this->db->insert('class',$data);
		$exam_id=$this->db->insert_id();
		echo $exam_id;
	
	}
	/*class delete function
	 *
	 */
	public function  delete(){
		$user_id=$this->input->post('id');
	
		$this->db->where('id',$user_id);
		$this->db->delete('class');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	
	}
	
	/* Load the class-  edit function
	 *
	 */
	
	public function edit()
	{
		$id=$this->input->post('id');
		$data['result']=$this->class_model->claslist_edit($id);
		$data['teacher_list']=$this->addteacher_model->teacherlist();
		$this->load->view('ajax_class_edit',$data);
	}
	
	
	/* class  update function
	 *
	 */
	
	public function update()
	{
	
		$rev_id=$this->input->post('rev_id');
		$clas_name=$this->input->post('name');
		$teacher=$this->input->post('teacher');
		$description=$this->input->post('description');
			
		$data=array(
				'id'=>$rev_id,
				'name'=>$clas_name,
				'class_teacher'=>$teacher,
				'descriptin'=>$description
		);
			
		$this->db->where('id', $rev_id);
		$this->db->update('class', $data);
	
		echo($rev_id);
			
	}

	
}