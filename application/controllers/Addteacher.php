<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Addteacher extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url','html','date'));
		$this->load->library(array('form_validation','session'));
		$this->load->model('addteacher_model');

	}

	/* Load the Add teacher page
	 *
	 */

	
	public function  index(){


		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$this->load->view('head',$data);
		$this->load->view('addteacher',$data);
		$this->load->view('footer',$data);
	}
	
	/*teacher insert function
	 *
	 */
	
	
	public function  insert(){
	
		$teacher_name=$this->input->post('name');
		$mail=$this->input->post('email');
		$username=$this->input->post('uname');
		$password=md5($this->input->post('password'));
		$mobile=$this->input->post('mobile');
		
		
		$data=array(
				
				'name'=>$teacher_name,
				'email'=>$mail,
				'username'=>$username,
				'password'=>md5($password),
				'mobile_number'=>$mobile
				
		);
		
		
		
		$this->db->insert('teacher',$data);
		$exam_id=$this->db->insert_id();
		echo $exam_id;
		 


	}
	/* retrive teacher list function
	 *
	 */
	
	public function  getlist(){
		$data['username']=$this->session->userdata('username');
		$data['id']	     =$this->session->userdata('id');
		$data['teacher_list']=$this->addteacher_model->teacherlist();
		$this->load->view('head',$data);
		$this->load->view('teacherlist',$data);
		$this->load->view('footer',$data);
				
	}
	/* teacher-  delete function
	 *
	 */
	
	public function  delete(){
		$user_id=$this->input->post('id');
		
		$this->db->where('id',$user_id);
		$this->db->delete('teacher');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	
	}
	
	/* Load the teacher-  edit function
	 *
	 */
	
	public function edit()
	{
		$id=$this->input->post('id');
		$data['result']=$this->addteacher_model->teacherlist_edit($id);
		$this->load->view('ajax_edit_teacher',$data);
	}
	
	
	/* teacher-  update function
	 *
	 */
	
	public function update()
	{
		
			$rev_id=$this->input->post('rev_id');
			$teacher_name=$this->input->post('name');
			$mail=$this->input->post('email');
			$uname=$this->input->post('uname');
			$pass=md5($this->input->post('password'));
			$mobile=$this->input->post('mobile');
			
			
			$data=array(
					'id'=>$rev_id,
					'name'=>$teacher_name,
					'email'=>$mail,
					'username'=>$uname,
					'password'=>md5($pass),
					'mobile_number'=>$mobile,
					
			);
			
			$this->db->where('id', $rev_id);
			$this->db->update('teacher', $data);
		
			echo($rev_id);
			
	}
	}

?>