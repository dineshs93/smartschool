<script type="text/javascript">
$(document).ajaxStart(function() { Pace.restart(); });
$(document).ready(function() { 
	
	$('#question_table').DataTable({
		drawCallback: function() {
			 $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
			      checkboxClass: 'icheckbox_flat-green',
			      radioClass: 'iradio_flat-green'
			    });

		  }    
       
    });
	$("#text_answer").hide();
	$("#option_answer").hide();
	$("#error_alert").hide();
	$("#error_alert1").hide();

	
/*
 * question modal show
 */

	$(".create_exam").click(function(){

		  $("#question_modal").modal();
		  

		});
	

	/*
	 * End
	 */

	 $( 'textarea.editor').each( function() {

		
		    CKEDITOR.replace( $(this).attr('id') );

		});

	

	  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });

	    /*
	    *Onchange function to answer type 
	    */

	    $("#answer_type").change(function(){

			var value=$(this).val();

	    	if(value ==0){
	    		$("#text_answer").hide();
	    		$("#option_answer").hide();
            
	    	}else if(value ==1){
	    		$("#text_answer").show();
	    		$("#option_answer").hide();
	    	}else if (value==2){
	    		$("#text_answer").hide();
	    		$("#option_answer").show();

	    	}

	    });


	    /*
		Start : Insert Question details with validation 
		*/
		
	$("#create_question").click(function(){

		
		var content = CKEDITOR.instances['editor2'].getData();
		
	    var mark=$("#set_mark").val();
	    var ans_type=$("#answer_type").val();
	    
	    var text_ans=$("#text_ans").val();
	    var correct_answer = $("input[name='r3']:checked").val();
	    var optiona=$("#optiona").val();
	    var optionb=$("#optionb").val();
	    var optionc=$("#optionc").val();
	    var optiond=$("#optiond").val();
	    var exam_id=$("#exam_id").val();
	    
		if(content=="" || mark == "" || ans_type == "0"){
			if(content==""){
				
			$(".question_name").addClass("has-error");
			}

			if(mark == ""){
				
				$(".set_mark").addClass("has-error");
			}
			
			if(ans_type == "0"){
				
				$(".answer_type").addClass("has-error");
			}
			
		}else if(ans_type == 1){

			if(text_ans == "" ){
           $("#text_answer").addClass("has-error");
			}else{
				var dataString='content='+encodeURIComponent(content)+'&mark='+mark+'&ans_type='+ans_type+'&text_ans='+text_ans+'&correct_answer='+correct_answer+'&optiona='+optiona+'&optionb='+optionb+'&optionc='+optionc+'&optiond='+optiond+'&exam_id='+exam_id;
		        
				
				$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>exam/question_insert/ ",
				data:dataString  ,
				success: function(data){
					
					if(data == 1){
					location.reload(true);
					}else{
						alert("please try again later");
					}
				},
			    error: function(jqXHR, textStatus) {
			        alert( "Request failed: " + jqXHR );
			    }
				});

			}
		}else if(ans_type == 2){

          if(typeof correct_answer == "undefined" || (optiona == "" || optionb == "" || optionc == "" || optiond == "")){
			if(typeof correct_answer == "undefined" && ans_type == 2){
				$(".error_crt_answer").addClass("alert alert-danger").html("check correct answer option");
				$("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
		            $("#error_alert").slideUp(500);
		             }); 
			}

			if((optiona == "" || optionb == "" || optionc == "" || optiond == "") && ans_type == 2){
				$(".error_option_answer").addClass("alert alert-danger").html("Plase fill all options");
				$("#error_alert1").fadeTo(2000, 500).slideUp(500, function(){
		            $("#error_alert1").slideUp(500);
		             }); 
			}
          }else{
			var dataString='content='+content+'&mark='+mark+'&ans_type='+ans_type+'&text_ans='+text_ans+'&correct_answer='+correct_answer+'&optiona='+optiona+'&optionb='+optionb+'&optionc='+optionc+'&optiond='+optiond+'&exam_id='+exam_id;
	        
			
			$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>exam/question_insert/ ",
			data:dataString  ,
			success: function(data){
				
				if(data == 1){
					location.reload();
					}else{
						alert("please try again later");
					}
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
			});
		  
		}
		}
	});


	$("textarea #editor2").focusout( function(){

		var content = CKEDITOR.instances['editor2'].getData();
		if(content !=''){
	    $(".question_name").removeClass("has-error");
	    $(".question_name").addClass("has-success");
		}else{
		$(".question_name").addClass("has-error");
		}
	});

	$('#set_mark').focusout( function(){

		var set_mark=$("#set_mark").val();
		if(set_mark !=''){
	    $(".set_mark").removeClass("has-error");
	    $(".set_mark").addClass("has-success");
		}else{
		$(".set_mark").addClass("has-error");
		}
	});

	$('#answer_type').change( function(){

		var answer_type=$("#answer_type").val();
		if(answer_type !=0){
	    $(".answer_type").removeClass("has-error");
	    $(".answer_type").addClass("has-success");
		}else{
		$(".answer_type").addClass("has-error");
		}
	});

	$('#text_answer').focusout( function(){

		var text_answer=$("#text_ans").val();
		if(text_answer !=""){
	    $("#text_answer").removeClass("has-error");
	    $("#text_answer").addClass("has-success");
		}else{
		$("#text_answer").addClass("has-error");
		}
	});

	/*
	*Edit option for question
	*/

	$("#question_table").on('click', '.question_edit', function(){
		

		var question_id=$(this).attr('id');
		var dataString="id="+question_id;
		

		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>exam/question_edit/ ",
			data:dataString  ,
			success: function(data){
			
				$("#edit_result").html(data);
				$("#edit_text_answer").hide();
				$("#edit_option_answer").hide();
				$("#edit_error_alert").hide();
				$("#edit_error_alert1").hide();

				if($("#edit_answer_type").val()==1){
					$("#edit_text_answer").show();
		    		$("#edit_option_answer").hide();
				}else{
					$("#edit_text_answer").hide();
		    		$("#edit_option_answer").show();
				}

				 $("#edit_answer_type").change(function(){

						var value=$(this).val();

				    	if(value ==0){
				    		$("#edit_text_answer").hide();
				    		$("#edit_option_answer").hide();
			            
				    	}else if(value ==1){
				    		$("#edit_text_answer").show();
				    		$("#edit_option_answer").hide();
				    	}else if (value==2){
				    		$("#edit_text_answer").hide();
				    		$("#edit_option_answer").show();

				    	}

				    });
				 $('input[type="checkbox"].flat-red, input[type="radio"].edit_flat-green').iCheck({
				      checkboxClass: 'icheckbox_flat-green',
				      radioClass: 'iradio_flat-green'
				    });		
							
				$( 'textarea.editor').each( function() {
				    CKEDITOR.replace( 'editor1');

				});

				



				
					
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
			});
		  
		

			
		
		
	});


	$(document).on('click', '#edit_question', function(){

	   var edit_content = CKEDITOR.instances['editor1'].getData();
	 
	    var edit_mark=$("#edit_set_mark").val();
	    var edit_ans_type=$("#edit_answer_type").val();
	    
	    var edit_text_ans=$("#edit_text_ans").val();
	    var edit_correct_answer = $("input[name='r3']:checked").val();
	    var edit_optiona=$("#edit_optiona").val();
	    var edit_optionb=$("#edit_optionb").val();
	    var edit_optionc=$("#edit_optionc").val();
	    var edit_optiond=$("#edit_optiond").val();
	    var edit_question_id=$("#edit_question_id").val();
	    var edit_exam_id=$("#edit_exam_id").val();
		if(edit_content=="" || edit_mark == "" || edit_ans_type == "0"){
			if(edit_content==""){
				
			$(".edit_question_name").addClass("has-error");
			}

			if(edit_mark == ""){
				
				$(".edit_set_mark").addClass("has-error");
			}
			
			if(edit_ans_type == "0"){
				
				$(".edit_answer_type").addClass("has-error");
			}
			
		}else if(edit_ans_type == 1){

			if(edit_text_ans == "" ){
           $("#edit_text_answer").addClass("has-error");
			}else{
				var dataString='edit_content='+encodeURIComponent(edit_content)+'&edit_mark='+edit_mark+'&edit_ans_type='+edit_ans_type+'&edit_text_ans='+edit_text_ans+'&edit_correct_answer='+edit_correct_answer+'&edit_optiona='+edit_optiona+'&edit_optionb='+edit_optionb+'&edit_optionc='+edit_optionc+'&edit_optiond='+edit_optiond+'&edit_question_id='+edit_question_id+'&edit_exam_id='+edit_exam_id;
		        
				
				$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>exam/question_update/ ",
				data:dataString  ,
				
				success: function(data){
					
					if(data == 1){
					location.reload(true);
					}else{
						alert("please try again later");
					}
				},
			    error: function(jqXHR, textStatus) {
			        alert( "Request failed: " + jqXHR );
			    }
				});

			}
		}else if(edit_ans_type == 2){

          if(typeof edit_correct_answer == "undefined" || (edit_optiona == "" || edit_optionb == "" || edit_optionc == "" || edit_optiond == "")){
			if(typeof edit_correct_answer == "undefined" && edit_ans_type == 2){
				$(".edit_error_crt_answer").addClass("alert alert-danger").html("check correct answer option");
				$("#edit_error_alert").fadeTo(2000, 500).slideUp(500, function(){
		            $("#edit_error_alert").slideUp(500);
		             }); 
			}

			if((edit_optiona == "" || edit_optionb == "" || edit_optionc == "" || edit_optiond == "") && edit_ans_type == 2){
				$(".edit_error_option_answer").addClass("alert alert-danger").html("Plase fill all options");
				$("#edit_error_alert1").fadeTo(2000, 500).slideUp(500, function(){
		            $("#edit_error_alert1").slideUp(500);
		             }); 
			}
          }else{
				var dataString='edit_content='+edit_content+'&edit_mark='+edit_mark+'&edit_ans_type='+edit_ans_type+'&edit_text_ans='+edit_text_ans+'&edit_correct_answer='+edit_correct_answer+'&edit_optiona='+edit_optiona+'&edit_optionb='+edit_optionb+'&edit_optionc='+edit_optionc+'&edit_optiond='+edit_optiond+'&edit_question_id='+edit_question_id+'&edit_exam_id='+edit_exam_id;;
	        
			
			$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>exam/question_update/ ",
			data:dataString  ,
			success: function(data){
				alert(data);
				if(data == 1){
					location.reload();
					}else{
						location.reload();
					}
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
			});
		  
		}
		}
	     
		});


	$('#question_table').on('click', '.question_delete', function(){
		event.preventDefault();
		var r = confirm("Are you sure want to delete this question ?");
		if (r == true){
			 $(this).closest('tr').hide();
	var question_id=$(this).attr('id');

	var dataString='question_id='+question_id;
	$.ajax({
	type: "post",
	url:"<?php echo base_url(); ?>/exam/question_delete/",
	data:dataString  ,
	success: function(data){
		
		alert("Sucessfully Deleted");
	},
	error: function(jqXHR, textStatus) {
	    alert( "Request failed: " + jqXHR );
	}
	});
		}

	});
	    
});

</script>



<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Questions <small>Smart School</small>


		</h1>

	</section>
	<div class="row">
		<div class="col-md-4"></div>
		<!-- <div class="col-md-6"><button class="btn btn-success create_exam">Add Question</button> 
	</div> -->
	</div>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Exam Details</h3>



				<div class="box-tools pull-right">

					<button type="button" class="btn btn-box-tool"
						data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"
						data-toggle="tooltip" title="Remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Exam Name :</label> <input type="text"
								class="form-control" value="<?php echo $exam_detail->name;?>"
								disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Duration :</label> <input type="text" class="form-control"
								value="<?php echo $exam_detail->duration;?>" disabled>
						</div>
					</div>
				</div>

	<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Total Mark: </label> <input type="text"
								class="form-control"
								value="<?php echo $exam_detail->total_mark;?>" disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Assigned Mark :</label> <input type="text"
								class="form-control"
								value="<?php echo $exam_detail->assigned_mark;?>" disabled>
						</div>
					</div>


				</div>

				<h5 style="color: red">***Note: if total mark and assigned mark not
					equal means exam is not activate make ensure. **</h5>

			</div>
			<!-- /.box-body -->

			<!-- /.box-footer-->
		</div>

<div class="box-tools">
							<button class="btn btn-success create_exam">Add Question</button>
						</div>
						<br>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Questions Details</h3>

						
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive padding">
						<table class="table table-bordered" border=1 id="question_table">
							 <thead>
							<tr>
								<th>S.No</th>
								<th>Question</th>
								<th>Mark</th>
								<th>Edit</th>
								<th>Delete</th>

							</tr>
							 </thead>
							 <tbody>
							<?php
							$sno = 0;
							foreach ( $question_detail as $row ) {
								$sno ++;
								?>
							
							
							
							
							<tr>
								<td><?php echo $sno;?></td>
								<td>
								<?php $question= $row["question"];
								echo utf8_decode($question);
								?>
								<br> <b>Answer:</b> <?php
								
								if ($row ["type"] == 1) {
									
									?><input class="form-control" type="text"
									value="<?php echo utf8_decode($row["answer"]);?>" disabled>
									
									
									
									<?php }elseif ($row["type"]==2){?>
									<table class="table table-striped">
										<tr>
											<th style="width: 10px">Options</th>
											<th style="width: 30px">Coreect Answer</th>
											<th>Answer</th>

										</tr>
										<tr>
											<td>a.</td>
											<td>
											<?php if($row["option_ans"]=="optiona"){?>
											<input type="radio" name="<?php echo $row["id"];?>r3" class="flat-red"
												value="optiona" checked><?php }else{?><input type="radio"
												name="<?php echo $row["id"];?>r3" class="flat-red" value="optiona" disabled><?php }?></td>

											<td><input type="text" class="form-control" id="<?php echo $row["id"];?>optiona"
												value="<?php echo $row["optiona"];?>" name="optiona"
												disabled></td>

										</tr>
										<tr>
											<td>b.</td>

											<td>
											<?php if($row["option_ans"]=="optionb"){?>
											<input type="radio" name="<?php echo $row["id"];?>r3" class="flat-red"
												value="optionb" checked><?php }else{?><input type="radio"
												name="<?php echo $row["id"];?>r3" class="flat-red" value="optionb" disabled><?php }?>
												
												</td>
											<td><input type="text" class="form-control" id="<?php echo $row["id"];?>optionb"
												value="<?php echo $row["optionb"];?>" name="optionb"
												disabled></td>

										</tr>
										<tr>
											<td>c.</td>
											<td><?php if($row["option_ans"]=="optionc"){?>
											<input type="radio" name="<?php echo $row["id"];?>r3" class="flat-red"
												value="optionc" checked><?php }else{?><input type="radio"
												name="<?php echo $row["id"];?>r3" class="flat-red" value="optionc" disabled><?php }?></td>
											<td><input type="text" class="form-control" id="<?php echo $row["id"];?>optionc"
												value="<?php echo $row["optionc"];?>" name="optionc"
												disabled></td>

										</tr>
										<tr>
											<td>d.</td>
											<td><?php if($row["option_ans"]=="optiond"){?>
											<input type="radio" name="<?php echo $row["id"];?>r3" class="flat-red"
												value="optiond" checked><?php }else{?><input type="radio"
												name="<?php echo $row["id"];?>r3" class="flat-red" value="optiond" disabled><?php }?></td>
											<td><input type="text" class="form-control" id="<?php echo $row["id"];?>optiond"
												value="<?php echo $row["optiond"];?>" name="optiond"
												disabled></td>

										</tr>

									</table>
									<?php }?>
									
									
								
								
								
								
								</td>
								<td><?php echo $row["mark"];?></td>
								<td><p data-placement="left" data-toggle="tooltip" title="Edit">
										<a id="<?php echo $row["id"];?>" class="btn btn-primary btn-xs question_edit" data-title="Edit"
											data-toggle="modal" data-target="#edit_modal">
											<span class="glyphicon glyphicon-pencil"></span>
										</a>
									</p></td>
								<td>
									<p data-placement="right" data-toggle="tooltip" title="Delete"
										style="float: left;">
										<a id="<?php echo $row["id"];?>" class="btn btn-danger btn-xs question_delete" data-title="Delete"
											data-toggle="modal" data-target="#delete">
											<span class="glyphicon glyphicon-trash"></span>
										</a>
									</p>
								</td>


							</tr>
							<?php }?>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>

<!-- Modal for add question  -->

<div class="modal fade" id="question_modal" tabindex="-1" role="dialog"
	aria-labelledby="question_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Add Question</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<!-- Nested Container Starts -->
				<div class="container-fluid">


					<div class="row">
						<form>
							<input type="hidden" id="exam_id"
								value="<?php echo $exam_detail->id;?>">
							<div class="box-body">
								<div class="form-group question_name">
									<label for="question">Question :</label>
									<textarea class="editor" id="editor2" name="editor2" rows="2"
										cols="30">
                                            
                    </textarea>
								</div>





								<div class="row">
									<div class="col-md-6">
										<div class="form-group set_mark">
											<label>Set Mark:</label> <input type="number"
												class="form-control" id="set_mark">

										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group answer_type">
											<label>Answer Type:</label> <select class="form-control "
												id="answer_type">
												<option value="0">Choose type</option>
												<option value="1">Text mode</option>
												<option value="2">Optional mode</option>
											</select>

										</div>
									</div>
								</div>

								<div class="form-group " id="text_answer">
									<label>Answer :</label> <input type="text" class="form-control"
										name="text_answer" id="text_ans">
								</div>




								<div class="form-group" id="option_answer">
									<div class="alert alert-danger" id="error_alert">
										<button type="button" class="close" data-dismiss="alert">x</button>
										<strong class="error_crt_answer"> </strong>
									</div>

									<div class="alert alert-danger" id="error_alert1">
										<button type="button" class="close" data-dismiss="alert">x</button>
										<strong class="error_option_answer"> </strong>
									</div>
									<table class="table table-striped">
										<tr>
											<th style="width: 10px">Options</th>
											<th style="width: 30px">Coreect Answer</th>
											<th>Answer</th>

										</tr>
										<tr>
											<td>a.</td>
											<td><input type="radio" name="r3" class="flat-red"
												value="optiona"></td>
											<td><input type="text" class="form-control" id="optiona"
												name="optiona"></td>

										</tr>
										<tr>
											<td>b.</td>
											<td><input type="radio" name="r3" class="flat-red"
												value="optionb"></td>
											<td><input type="text" class="form-control" id="optionb"
												name="optionb"></td>

										</tr>
										<tr>
											<td>c.</td>
											<td><input type="radio" name="r3" class="flat-red"
												value="optionc"></td>
											<td><input type="text" class="form-control" id="optionc"
												name="optionc"></td>

										</tr>
										<tr>
											<td>d.</td>
											<td><input type="radio" name="r3" class="flat-red"
												value="optiond"></td>
											<td><input type="text" class="form-control" id="optiond"
												name="optiond"></td>

										</tr>

									</table>

								</div>



							</div>
							<!-- /.box-body -->

							<div class="modal-footer">


								<button type="button" id="create_question"
									class="btn btn-success">Save</button>


							</div>
						</form>



					</div>
				</div>
				<!-- Nested Container Ends -->
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>

<!--  -->

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog"
	aria-labelledby="edit_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Edit Question</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<div id="edit_result"></div>
				<!-- Nested Container Ends -->
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>





