<script type="text/javascript">
$(document).ready(function() {

	 $('#confirm_button').prop('disabled', true);
	
	$("#exam_list").on('click', '.take_exam', function(){

		var id=$(this).attr('id');

		var dataString='id='+id;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>student_exam/exam_instructions",
			data:dataString,
			success: function(data){
				$("#exam_result").html(data);
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
			});
	});


	$('#agree').change(function(){
		if($('#agree').is(':checked')){
			$('#confirm_button').prop('disabled', false);
		    } else {
		    $('#confirm_button').prop('disabled', true);
		    }
		});

	$("#confirm_button").click(function(){
 
    var exam_id=$("#exam_id").val();
    window.location.href="<?php echo base_url();?>student_exam/exam/"+exam_id;
    
		
	});


	 $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });

});

</script>







<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Student Exam List <small>Smart School</small>


		</h1>

	</section>
	<div class="row">
		<div class="col-md-4"></div>
		<!-- <div class="col-md-6"><button class="btn btn-success create_exam">Add Question</button> 
	</div> -->
	</div>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Exam List</h3>
			</div>
			
			<div class="box-body">
				<table class="table table-striped"  id="exam_list">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Exam name</th>
							<th>Duration (MINS)</th>
							<th>Total Mark</th>
							<th>Pass mark</th>
							<th width="350px">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php $sno = 0;
							foreach ( $exam_list as $row ) {
								$sno ++;
								?>
						<tr>
							<td><?php echo $sno;?></td>
							<td><?php echo $row['name'];?></td>
							
							<td><?php echo $row['duration'];?> Mins</td>
							<td><?php echo $row['total_mark'];?></td>
							<td><?php echo $row['pass_mark'];?></td>
							<td><a id="<?php echo $row['id'];?>"
								class="btn btn-success btn-sm take_exam" data-toggle="modal"
								data-target="#take_exam"> <i
									class="glyphicon glyphicon-edit icon-white"></i> Take Exam
							</a></td>

						</tr>
						<?php }?>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->
			</div>
			</section>
			
			 
			</div>
			
			
			
			
			<!--  -->

<div class="modal fade" id="take_exam" tabindex="-1" role="dialog"
	aria-labelledby="exam_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Instructions</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<div id="exam_result"></div>
				<!-- Nested Container Ends -->
			</div>
			<div class="modal-footer">
			<div class="" style="float:left !important;"><input type="checkbox" name="agree" id="agree" value=""> &nbsp;I agree with Instructions.</div>
			
    <button type="button" data-dismiss="modal" class="btn btn-success" id="confirm_button">Start Exam</button>
    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
  </div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>
			
			