<script type="text/javascript">
$(function() {
	function isValidEmailAddress(mail) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(mail);
};
	

	/*
	Start : Intializaton part
	*/

	$(".assign_to").select2();
	
	$('#reservation').daterangepicker();


	/*
	End : Intializaton part
	*/


	/*
	Start : Insert teacher details with validation 
	*/
$("#create_teacher").click(function(){

	var teacher_name=$("#teacher_name").val();
	var mail=$("#mail").val();
	var username=$("#username").val();
    var pass=$("#pass").val();
    var mobile=$("#mobile").val();
    var cpass=$("#cpass").val();
    var rev_id = $('#rev_id').val();
   
    
    if(teacher_name=="" || pass == "" || mail == "" || username == "" || mobile == ""){
		if(teacher_name==""){
		$(".teacher_name").addClass("has-error");
		}
		if(mail==""){
			$(".mail").addClass("has-error");
			}
		if(username==""){
			$(".username").addClass("has-error");
			}
		if(pass == ""){
			$(".pass").addClass("has-error");
		}
		if(mobile == ""){
			$(".mobile").addClass("has-error");
		}
		if(cpass == ""){
			$(".cpass").addClass("has-error");
		}
		
	}
	else if (pass != cpass) {  $('.pass').addClass('has-error');
    $('.cpass').addClass('has-error');}
	else if (!isValidEmailAddress(mail)) {  $('.mail').addClass('has-error');
   }
	else{    

		 dataString='name='+teacher_name+'&email='+mail+'&uname='+username+'&password='+pass+'&mobile='+mobile+'&cpass='+cpass;
						
		$.ajax({
		
		type: "post",
		url:"<?php echo base_url(); ?>addteacher/insert/ ",
		data:dataString  ,
		success: function(data){
			
			window.location.href="<?php echo base_url();?>addteacher/getlist/";
		},
	    error: function(jqXHR, textStatus) {
	        alert( "Request failed: " + jqXHR );
	    }
		});
		
	}

});
	
$('#teacher_name').focusout( function(){

	var teacher_name=$("#teacher_name").val();
	if(teacher_name !=''){
    $(".teacher_name").removeClass("has-error");
    $(".teacher_name").addClass("has-success");
	}else{
	$(".teacher_name").addClass("has-error");
	}
});
$('#mail').focusout( function(){

	var mail=$("#mail").val();
	if(mail !=''){
    $(".mail").removeClass("has-error");
    $(".mail").addClass("has-success");
	}else{
	$(".mail").addClass("has-error");
	}
});
$('#username').focusout( function(){

	var username=$("#username").val();
	if(username !=''){
    $(".username").removeClass("has-error");
    $(".username").addClass("has-success");
	}else{
	$(".username").addClass("has-error");
	}
});

$('#duration').focusout( function(){

	var pass=$("#pass").val();
	if(pass !=''){
    $(".pass").removeClass("has-error");
    $(".pass").addClass("has-success");
	}else{
	$(".pass").addClass("has-error");
	}
});

$('#mobile').focusout( function(){

	var mobile=$("#mobile").val();
	if(mobile !=''){
    $(".mobile").removeClass("has-error");
    $(".mobile").addClass("has-success");
	}else{
	$(".mobile").addClass("has-error");
	}
});
$('#cpass').focusout( function(){

	var cpass=$("#cpass").val();
	if(cpass !=''){
    $(".cpass").removeClass("has-error");
    $(".cpass").addClass("has-success");
	}else{
	$(".cpass").addClass("has-error");
	}
});

/*
End : Insert teacher details with validation 
*/

	
});
</script>



<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Teacher <small>Smart School</small>
		</h1>
 <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add New Teacher</li>
      </ol>
	</section>

	<!-- Main content -->
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Teacher</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<div class="form-group teacher_name">
								<label for="teacher_name">Teacher name :</label> <input type="text"
									class="form-control " id="teacher_name" name="inputName"
									placeholder="Enter Full Name">
							</div>
<div class="form-group mail">
								<label for="mail">Email :</label> <input type="email"
									class="form-control " id="mail" name="inputEmail"
									placeholder="Enter Teacher's Email">
							</div>
<div class="form-group username">
								<label for="username">User name :</label> <input type="text"
									class="form-control " id="username" name="inputUName"
									placeholder="Enter User Name">
							</div>
							<div class="form-group pass">
								<label for="pass">Password :</label> <input type="password"
									class="form-control " id="pass" name="inputPassword"
									placeholder="Enter Password">
							</div>
							<div class="form-group cpass">
								<label for="cpass">Confirm Password :</label> <input type="password"
									class="form-control " id="cpass" name="inputPassword"
									placeholder="Enter Password">
							</div>

							<div class="form-group mobile">
								<label>Mobile:</label>

								<div class="input-group">
								<div class="input-group-addon">
										<i class="fa fa-mobile"></i>
									</div>
									<input type="number" class="form-control" name="inputMobile" id="mobile"> 
								</div>
							</div>

							
							

						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="create_teacher" class="btn btn-success">Add
								Teacher</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>

