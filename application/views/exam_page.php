<script type="text/javascript">
$(document).ready(function() {

	 $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass: 'iradio_flat-green'
	    });	
	
	 window.onbeforeunload = function (evt) {
		  var message = 'Are you sure you want to leave?';
		  if (typeof evt == 'undefined') {
		    evt = window.event;
		  }
		  if (evt) {
		    evt.returnValue = message;
		  }
		  return message;
		}

	var fixmeTop = $('.exam_head').offset().top;
	$(window).scroll(function() {
	    var currentScroll = $(window).scrollTop();
	    if (currentScroll >= fixmeTop) {
	        $('.exam_head').css({
	            position: 'fixed',
	            top:'0'
	           
	        });
	    } else {
	        $('.exam_head').css({
	            position: 'relative'
	        });
	    }
	});

	var numItems = $('.item').length;
	
	var first=1;
	var last=numItems;
	
	
	$("#question_"+first).addClass("active");
    $("#question_"+first).removeClass("deactive");
    
    $('.previous').prop('disabled', true);

    $(".previous").click(function(){

        var p_id=$(this).attr("id");
        var current=parseInt(p_id) - parseInt(1);
    	var next=parseInt(current) + parseInt(1);
    	if(current == first){
    		$('.previous').prop('disabled', true);
    		$('.next').prop('disabled', false);
    	}else{
    		$('.next').prop('disabled', false);
    	}
    	$("#question_"+next).addClass("deactive");
    	$("#question_"+current).addClass("active");
        $("#question_"+current).removeClass("deactive");
        

    });

    $(".next").click(function(){

    	var n_id=$(this).attr("id");
    	var current=parseInt(n_id) + parseInt(1);
    	var previous=parseInt(current) - parseInt(1);
    	if(current == last){
    		$('.next').prop('disabled', true);
    		$('.previous').prop('disabled', false);
    	}else{
    		$('.previous').prop('disabled', false);
    	}
    	$("#question_"+previous).addClass("deactive");
    	$("#question_"+current).addClass("active");
        $("#question_"+current).removeClass("deactive");

    });



    <!--Exam insert function after click finish exam- -->

    $(".exam_submit").on('click',function(){

    	var r = confirm("Are you sure to submit exam ?");
    	if(r ==true){
    	window.onbeforeunload=null;
		var exam_id=$("#exam_id").val();
		var std_id=$("#std_id").val();
		
        var count=1;
        $(".item").each(function(){
        	
		var ques_type=$("#ques_type_"+count).val();

		if(ques_type == 1){

			var question_id=$(".ans_"+count).attr('id');
			var answer=$("#"+question_id).val();
			
		}else if(ques_type == 2){
			
			var question_id=$("input[name='"+count+"r3']").attr('id');
			
			var answer=$('input[name='+count+'r3]:checked').val()
			
		}

      var datastring='question_id='+question_id+'&answer='+answer+'&exam_id='+exam_id+'&std_id='+std_id;
      $.ajax({
    		type: "post",
    		url:"<?php echo base_url(); ?>/student_exam/answer_insertion/",
    		data:datastring,
    		async:false,
    		success: function(data){
    			
    			
    		},
    		error: function(jqXHR, textStatus) {
    		    alert( "Request failed: " + jqXHR );
    		}
    		});
      
    
		count++;

        });

        window.location.href="<?php echo base_url();?>student_exam/exam_summary/"+exam_id; 
    	}

    });
	
});
</script>


<style>
.active {
	display: block !important;
}

.deactive {
	display: none !important;
}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Exam<small>Smart School</small>


		</h1>

	</section>

	<section class="content ">
		<div class="row ">
			<div class="col-md-4">
				<h4></h4>

				<input type="hidden" id="exam_id"
					value="<?php echo $exam_details->id;?>"> <input type="hidden"
					id="std_id" value="<?php echo $_SESSION['id'];?>">
			</div>

		</div>
		<div class="row">
			<div class="col-md-8">
<?php
$sno = 0;
foreach ( $question_details as $row ) {
	$sno ++;
	?>

		<div class="box box-success box-solid item deactive"
					id="question_<?php echo $sno;?>">
					<div class="box-header with-border ">
						<h3 class="box-title">Question No : <?php echo $sno;?></h3>
					</div>

					<div class="box-body">
					
						<div class="row">
							<div class="col-md-12">
								<h4>
									<b>	<?php echo $row['question'];?></b>
								</h4>
							</div>

							<div class="col-md-12 ">
								Answer:<br> <br> <input type="hidden"
									id="ques_type_<?php echo $sno;?>"
									value="<?php echo $row["type"];?>">
									
				<?php
	if ($row ["type"] == 1) {
		?>
				
				<input type="text" class="form-control ans_<?php echo $sno;?>"
									id="<?php echo $row["id"];?>">
				
				<?php }else if($row["type"]==2){?>
				
				
				<table class="table table-striped">
									<tr>
										<td width="10px;"><input type="radio"
											name="<?php echo $sno;?>r3" id="<?php echo $row["id"];?>"
											value="optiona"></td>
										<td> <?php echo $row["optiona"];?></td>
									</tr>

									<tr>
										<td width="10px;"><input type="radio"
											name="<?php echo $sno;?>r3" id="<?php echo $row["id"];?>"
											value="optionb"></td>
										<td> 
										
										
										<?php echo $row["optionb"];?></td>
									</tr>

									<tr>
										<td width="10px;"><input type="radio"
											name="<?php echo $sno;?>r3" id="<?php echo $row["id"];?>"
											value="optionc"></td>
										<td> <?php echo $row["optionc"];?></td>
									</tr>

									<tr>
										<td width="10px;"><input type="radio"
											name="<?php echo $sno;?>r3" id="<?php echo $row["id"];?>"
											value="optiond"></td>
										<td> <?php echo $row["optiond"];?></td>
									</tr>
								</table>
								
				<?php }?>
				
				</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-5">
								<button class="btn btn-info previous" id="<?php echo $sno;?>">
									Previous</button>
								<button class="btn btn-info next" id="<?php echo $sno;?>">Next</button>
							</div>
							<div class="col-md-3">
								<button class="btn btn-success exam_submit" id="exam_submit">Finish Exam</button>
							</div>
						</div>
					</div>
				</div>
			
			<?php }?>
			</div>

			<div class="col-md-4">

				<div class="box box-info box-solid">
					<div class="box-header with-border ">
						<h3 class="box-title">Exam Details</h3>
					</div>

					<div class="box-body">
						Time :&nbsp;<span class="exam_head" id="timer"><input id="minutes"
							type="text"
							style="width: 20px; border: none; background-color: white; font-size: 20px; color: red !important; font-weight: bold;"
							disabled> mins <input id="seconds" type="text"
							style="width: 20px; color: red !important; border: none; background-color: white; font-size: 20px; font-weight: bold;"
							disabled> seconds.</span> <br> <br> Exam Name : <b><?php echo $exam_details->name;?></b><br>
						<br> Total Mark : <b><?php echo $exam_details->total_mark;?></b><br>
						<br>

					</div>
					<!-- /.box-body -->



				</div>
			</div>
		</div>
	</section>


</div>




<script type="text/javascript">
// set minutes
var mins = "<?php echo $exam_details->duration;?>";
 
// calculate the seconds (don't change this! unless time progresses at a different speed for you...)
var secs = mins*60;
function countdown() {
	setTimeout('Decrement()',1000);

	
}
function Decrement() {
	if (document.getElementById && secs > 0) {
		minutes = document.getElementById("minutes");
		seconds = document.getElementById("seconds");
		// if less than a minute remaining
		if (seconds < 59) {
			seconds.value = secs;
		} else {
			minutes.value = getminutes();
			seconds.value = getseconds();
		}
		secs--;
		setTimeout('Decrement()',1000);
	}
	
}
function getminutes() {
	// minutes is seconds divided by 60, rounded down
	mins = Math.floor(secs / 60);
	return mins;
}
function getseconds() {
	// take mins remaining (as seconds) away from total seconds remaining
	return secs-Math.round(mins *60);
}
</script>



<script>
countdown();
</script>