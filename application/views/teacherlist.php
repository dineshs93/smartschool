
<script> $(function() {
	$("#teacher_list").DataTable();
	$(document).on('click', '.save', function(){
		
		var teacher_name=$("#teacher_name").val();
		var mail=$("#mail").val();
		var username=$("#username").val();
	    var pass=$("#pass").val();
	    var mobile=$("#mobile").val();
	    var cpass=$("#cpass").val();
	    var rev_id = $('#rev_id').val();
	    if(teacher_name=="" || mail == "" || username == "" || mobile == ""){
			if(teacher_name==""){
			$(".teacher_name").addClass("has-error");
			}
			if(mail==""){
				$(".mail").addClass("has-error");
				}
			if(username==""){
				$(".username").addClass("has-error");
				}
			if(mobile == ""){
				$(".mobile").addClass("has-error");
			}
		}
		else{    
	    dataString='name='+teacher_name+'&email='+mail+'&uname='+username+'&mobile='+mobile+'&rev_id='+rev_id;
		
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/Addteacher/update/",
			data:dataString  ,
			success: function(data){
					
	        	alert("Data Updated");
	        	 window.location.reload();
	        	},
	            error: function(jqXHR, textStatus) {
	                alert( "Request failed: " + jqXHR );
	            }
	        	});
		}
	    });
	$('#teacher_list').on('click', '.edit', function(){

	       var id = $(this).attr('id');
	        
	        var dataString='id='+id;
	        $.ajax({
	        	type: "post",
	        	url:"<?php echo base_url(); ?>/Addteacher/edit/",
	        	data:dataString  ,
				timeout: 5000,
	        	success: function(data){
					
	        		$("#rev").html(data);
	        		
	        	},
	            error: function(jqXHR, textStatus) {
	                alert( "Request failed: " + jqXHR );
	            }
	        	});
			
	    });
	$('#teacher_list').on('click', '.delete', function(){
	event.preventDefault();
	var r = confirm("Are you sure to delete this user?");
	if (r == true){
		 $(this).closest('tr').hide();
var user_id=$(this).attr('id');

var dataString='id='+user_id;
$.ajax({
type: "post",
url:"<?php echo base_url(); ?>/Addteacher/delete/",
data:dataString  ,
success: function(data){
	
	alert("Sucessfully Deleted");
},
error: function(jqXHR, textStatus) {
    alert( "Request failed: " + jqXHR );
}
});
	}

});
	
$('#teacher_name').focusout( function(){

	var teacher_name=$("#teacher_name").val();
	if(teacher_name !=''){
    $(".teacher_name").removeClass("has-error");
    $(".teacher_name").addClass("has-success");
	}else{
	$(".teacher_name").addClass("has-error");
	}
});
$('#mail').focusout( function(){

	var mail=$("#mail").val();
	if(mail !=''){
    $(".mail").removeClass("has-error");
    $(".mail").addClass("has-success");
	}else{
	$(".mail").addClass("has-error");
	}
});
$('#username').focusout( function(){

	var username=$("#username").val();
	if(username !=''){
    $(".username").removeClass("has-error");
    $(".username").addClass("has-success");
	}else{
	$(".username").addClass("has-error");
	}
});

$('#duration').focusout( function(){

	var pass=$("#pass").val();
	if(pass !=''){
    $(".pass").removeClass("has-error");
    $(".pass").addClass("has-success");
	}else{
	$(".pass").addClass("has-error");
	}
});
 
$('#mobile').focusout( function(){

	var mobile=$("#mobile").val();
	if(mobile !=''){
    $(".mobile").removeClass("has-error");
    $(".mobile").addClass("has-success");
	}else{
	$(".mobile").addClass("has-error");
	}
});


/*
End : Insert exam details with validation 
*/

	
});
</script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Teachers Lists
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Teachers Lists</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="teacher_list" class="table table-bordered table-hover css-serial">
                <thead>
                
                <tr>
                <th>S.No</th>
                  <th>Teacher Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($teacher_list as $selected):
                $sno=0;
                $sno++;
                ?>
                <tr>
              
                <td><?php echo $sno;?></td>
                  <td><?php echo $selected->name; ?></td>
                  <td><?php echo $selected->email; ?></td>
                  <td><?php echo $selected->mobile_Number; ?></td>
                  <td align="center" ><a id="<?php echo $selected->id;?>" class="btn btn-info btn-sm edit" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-edit icon-white"></i>Edit </a>
												&nbsp; <a id="<?php echo $selected->id;?>" class="btn btn-danger btn-sm delete" ><i
													class="glyphicon glyphicon-trash icon-white"></i>Delete</a>
											</td>
                  
                </tr>
                
                <?php endforeach; ?> 
                 </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


 <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only">close</span>

				</button>
				<h4>Teacher Detail</h4>
			</div>
			<form method="post">


					<div class="reviews-form-box" id="rev"></div>
				
			

			</form>
		</div>
	</div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->

