  <script type="text/javascript">

$(document).ready(function() { 

  $("#teacher_submit").on('click',function(){
        
       var name=$("#teacher_name").val();
       var email=$("#teacher_mail").val();
       var uname=$("#teacher_uname").val();
       var mob_num=$("#teacher_mobile").val();
       var user_id=$("#user_id").val();

	  if(name == '' || email == '' || uname == '' || mob_num ==''){
          if(name == ''){
        	  $(".teacher_name").addClass("has-error");
          }

          if(email == ''){
        	  $(".teacher_mail").addClass("has-error");
          }
          if(uname == ''){
        	  $(".teacher_uname").addClass("has-error");
          }

          if(mob_num == ''){
        	  $(".teacher_mobile").addClass("has-error");
          }
 
	  }else{
          var dataString='name='+name+'&email='+email+'&uname='+uname+'&mob_num='+mob_num+'&user_id='+user_id;
          $.ajax({
              type:"post",
          	  url:"<?php echo base_url(); ?>Login/teacher_update/",
        	  data:dataString,
        	  
			  success: function(data){
					
					if(data == 1){
						alert("Updated successfully");
						location.reload(true);
						}else{
							alert("please try again later");
						}
				},
			    error: function(jqXHR, textStatus) {
			        alert( "Request failed: " + jqXHR );
			    }


          });
      

	  }

	

  });

  $("#std_submit").on('click',function(){
      
    var name=$("#student_name").val();
    var email=$("#student_mail").val();
    var uname=$("#student_uname").val();
    var mob_num=$("#student_mobile").val();
    var user_id=$("#user_id").val();

	  if(name == '' || email == '' || uname == '' || mob_num ==''){
       if(name == ''){
     	  $(".student_name").addClass("has-error");
       }

       if(email == ''){
     	  $(".student_mail").addClass("has-error");
       }
       if(uname == ''){
     	  $(".student_uname").addClass("has-error");
       }

       if(mob_num == ''){
     	  $(".student_mobile").addClass("has-error");
       }

	  }else{
       var dataString='name='+name+'&email='+email+'&uname='+uname+'&mob_num='+mob_num+'&user_id='+user_id;
       $.ajax({
           type:"post",
       	  url:"<?php echo base_url(); ?>Login/student_update/",
     	  data:dataString,
     	  
			  success: function(data){
					
					if(data == 1){
						alert("Updated successfully");
						location.reload(true);
						}else{
							alert("please try again later");
						}
				},
			    error: function(jqXHR, textStatus) {
			        alert( "Request failed: " + jqXHR );
			    }


       });
   

	  }

	

});


  $("#student_passsubmit").on('click',function(){

       var new_pass=$("#student_password").val();
       var con_pass=$("#student_password_confirm").val();

       if(new_pass =='' || con_pass==''){

    	   if(new_pass == ''){
    	     	  $(".student_password").addClass("has-error");
    	       }

    	   if(con_pass == ''){
    	     	  $(".student_password_confirm").addClass("has-error");
    	       }
     
       }else if (new_pass != con_pass){
		alert("Password does not match ");
       }else{
           var dataString='password='+new_pass;
           $.ajax({
			type:"post",
			url:"<?php echo base_url();?>login/student_passupdate/",
			data:dataString,
			success:function(data){
				if(data == 1){
					alert("Password updated successfully");
					location.reload(true);
					}else{
						alert("please try again later");
					}
			},
			error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
           });



       }

      
	  });

  $("#teacher_passsubmit").on('click',function(){

      var new_pass=$("#teacher_password").val();
      var con_pass=$("#teacher_password_confirm").val();

      if(new_pass =='' || con_pass==''){

   	   if(new_pass == ''){
   	     	  $(".teacher_password").addClass("has-error");
   	       }

   	   if(con_pass == ''){
   	     	  $(".teacher_password_confirm").addClass("has-error");
   	       }
    
      }else if (new_pass != con_pass){
		alert("Password does not match ");
      }else{
          var dataString='password='+new_pass;
          $.ajax({
			type:"post",
			url:"<?php echo base_url();?>login/teacher_passupdate/",
			data:dataString,
			success:function(data){
				if(data == 1){
					alert("Password updated successfully");
					location.reload(true);
					}else{
						alert("please try again later");
					}
			},
			error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
          });



      }

     
	  });
  

  $('#student_password').focusout( function(){
	  
	  var new_pass=$("#student_password").val();
		if(new_pass !=''){
	    $(".student_password").removeClass("has-error");
	    $(".student_password").addClass("has-success");
		}else{
		$(".student_password").addClass("has-error");
		}
	});

  $('#student_password_confirm').focusout( function(){
	  
	  var con_pass=$("#student_password_confirm").val();
		if(con_pass !=''){
	    $(".student_password_confirm").removeClass("has-error");
	    $(".student_password_confirm").addClass("has-success");
		}else{
		$(".student_password_confirm").addClass("has-error");
		}
	});

 $('#teacher_password').focusout( function(){
	  
	  var new_pass=$("#teacher_password").val();
		if(new_pass !=''){
	    $(".teacher_password").removeClass("has-error");
	    $(".teacher_password").addClass("has-success");
		}else{
		$(".teacher_password").addClass("has-error");
		}
	});

  $('#teacher_password_confirm').focusout( function(){
	  
	  var con_pass=$("#teacher_password_confirm").val();
		if(con_pass !=''){
	    $(".teacher_password_confirm").removeClass("has-error");
	    $(".teacher_password_confirm").addClass("has-success");
		}else{
		$(".teacher_password_confirm").addClass("has-error");
		}
	});

 
 


  $('#teacher_name').focusout( function(){
	  
	  		    var name=$("#teacher_name").val();
	  			if(name !=''){
	  		    $(".teacher_name").removeClass("has-error");
	  		    $(".teacher_name").addClass("has-success");
	  			}else{
	  			$(".teacher_name").addClass("has-error");
	  			}
	  		});

	  	  $("#teacher_mail").focusout( function(){

	  		    var email=$("#teacher_mail").val();
	  			if(email !=''){
	  		    $(".teacher_mail").removeClass("has-error");
	  		    $(".teacher_mail").addClass("has-success");
	  			}else{
	  			$(".teacher_mail").addClass("has-error");
	  			}
	  		});

	  	  $("#teacher_uname").focusout( function(){

	  		    var uname=$("#teacher_uname").val();
	  			if(uname !=''){
	  		    $(".teacher_uname").removeClass("has-error");
	  		    $(".teacher_uname").addClass("has-success");
	  			}else{
	  			$(".teacher_uname").addClass("has-error");
	  			}
	  		});

	  	  $("#teacher_mobile").focusout( function(){

	  		    var mob_num=$("#teacher_mobile").val();
	  			if(mob_num !=''){
	  		    $(".teacher_mobile").removeClass("has-error");
	  		    $(".teacher_mobile").addClass("has-success");
	  			}else{
	  			$(".teacher_mobile").addClass("has-error");
	  			}
	  		});


	  	$('#student_name').focusout( function(){
	  	  
  		    var name=$("#student_name").val();
  			if(name !=''){
  		    $(".student_name").removeClass("has-error");
  		    $(".student_name").addClass("has-success");
  			}else{
  			$(".student_name").addClass("has-error");
  			}
  		});

  	  $("#student_mail").focusout( function(){

  		    var email=$("#student_mail").val();
  			if(email !=''){
  		    $(".student_mail").removeClass("has-error");
  		    $(".student_mail").addClass("has-success");
  			}else{
  			$(".student_mail").addClass("has-error");
  			}
  		});

  	  $("#student_uname").focusout( function(){

  		    var uname=$("#student_uname").val();
  			if(uname !=''){
  		    $(".student_uname").removeClass("has-error");
  		    $(".student_uname").addClass("has-success");
  			}else{
  			$(".student_uname").addClass("has-error");
  			}
  		});

  	  $("#student_mobile").focusout( function(){

  		    var mob_num=$("#student_mobile").val();
  			if(mob_num !=''){
  		    $(".student_mobile").removeClass("has-error");
  		    $(".student_mobile").addClass("has-success");
  			}else{
  			$(".student_mobile").addClass("has-error");
  			}
  		});

  
});
</script>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php if($_SESSION['role'] == 1){?>
        Teacher Profile
        <?php }else{?>
          Student Profile
          <?php }?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       
        <li class="active"><?php if($_SESSION['role'] == 1){?>
        Teacher Profile
        <?php }else{?>
          Student Profile
          <?php }?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="http://p7cdn4static.sharpschool.com/UserFiles/Servers/Server_333558/Image/Back-to-School.jpg" alt="User profile picture">
<input type="hidden" value="<?php echo $_SESSION['id'];?>" id="user_id">
              <h3 class="profile-username text-center"><?php if($_SESSION['role'] == 1){
              	echo $user_detail->name;
       
       }else{
       	echo $user_detail->name;
       	
       }?></h3>

              <p class="text-muted text-center"><?php if($_SESSION['role'] == 1){
              	echo "Teacher";
       
       }else{
       	echo "Student";
       	
       }?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Mail</b> <a class="pull-right">
                  
                  <?php if($_SESSION['role'] == 1){
              	echo $user_detail->email;
               
       }else{
       	echo $user_detail->mail;
       	
       }?>
                  
                  </a>
                </li>
                 <?php if($_SESSION['role'] == 1){?>
                <li class="list-group-item">
                  <b>Mobile</b> <span class="pull-right">
                  
                  <?php 
              	echo $user_detail->mobile_Number;
               
       
       	
       ?>
                  
                  </span>
                </li>
               <?php }?>
                
                <li class="list-group-item">
                  <b>User Name</b> <span class="pull-right">
                  
                  <?php 
              	echo $user_detail->username;
               
       
       	
       ?>
                  
                  </span>
                </li>
               
              </ul>
              <?php if($_SESSION['role'] == 1){?>
              
              <a id="1" class="btn btn-primary btn-block " data-title="Password Change"
											data-toggle="modal" data-target="#teacher_modal">
											Change Password
										</a>
										<?php }else{?>
										<a id="2" class="btn btn-primary  btn-block" data-title="Password Change"
											data-toggle="modal" data-target="#student_modal">
											Change Password
										</a>
										<?php }?>
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              
              <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
             

              <div class=" active tab-pane" id="settings">
                
                <?php if($_SESSION['role'] == 1){?>
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10 teacher_name">
                      <input type="text" class="form-control" id="teacher_name" placeholder="Name" value="<?php echo $user_detail->name;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10 teacher_mail">
                      <input type="email" class="form-control" id="teacher_mail" placeholder="Email" value="<?php echo $user_detail->email;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">User Name</label>

                    <div class="col-sm-10 teacher_uname">
                      <input type="text" class="form-control" id="teacher_uname" placeholder="User name" value="<?php echo $user_detail->username;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Mobile Number</label>

                     <div class="col-sm-10 teacher_mobile">
                      <input type="number" class="form-control" id="teacher_mobile" placeholder="Mobile  number" value="<?php echo $user_detail->mobile_Number;?>">
                    </div>
                  </div>
                 
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <input type="button"  id="teacher_submit" class="btn btn-success" value="Update">
                    </div>
                  </div>
                </form>
                <br> <br> <br>
                <?php }?>
                
                  <?php if($_SESSION['role'] != 1){?>
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10 student_name">
                      <input type="text" class="form-control" id="student_name" placeholder="Name" value="<?php echo $user_detail->name;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10 student_mail">
                      <input type="email" class="form-control" id="student_mail" placeholder="Email" value="<?php echo $user_detail->mail;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">User Name</label>

                    <div class="col-sm-10 student_uname">
                      <input type="text" class="form-control" id="student_uname" placeholder="User name" value="<?php echo $user_detail->username;?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Mobile Number</label>

                     <div class="col-sm-10 student_mobile">
                      <input type="number" class="form-control" id="student_mobile" placeholder="Mobile  number" value="<?php echo $user_detail->mobile_number;?>">
                    </div>
                  </div>
                 
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <input type="button" id="std_submit" class="btn btn-success" value="Update">
                    </div>
                  </div>
                </form>
                <br>
                <?php }?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  
  
  
  
  <!---- Modal for student password change -->
  <div class="modal fade" id="student_modal" tabindex="-1" role="dialog"
	aria-labelledby="student_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Password Change</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group student_password">
							<label>Enter New Password: </label> <input type="text" id="student_password"
								class="form-control"
								>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group student_password_confirm">
							<label>Confirm Password :</label> <input type="password" id="student_password_confirm"
								class="form-control"
								>
						</div>
					</div>
<div class="col-md-12">
						<div class="form-group">
							<input type="button" id="student_passsubmit" class="btn btn-success" value="Change password">
						</div>
					</div>

				</div>
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>

<!---- Modal for student password change -->
  <div class="modal fade" id="teacher_modal" tabindex="-1" role="dialog"
	aria-labelledby="teacher_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Password Change</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Password: </label> <input type="text" id="teacher_password"
								class="form-control"
								>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Confirm Password :</label> <input type="password" id="teacher_password_confirm"
								class="form-control"
								>
						</div>
					</div>
<div class="col-md-12">
						<div class="form-group">
							<input type="button" id="teacher_passsubmit" class="btn btn-success" value="Change password">
						</div>
					</div>

				</div>
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>
   
   