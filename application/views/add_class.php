<style>.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}</style>
<script type="text/javascript">
$(function() {
	$("#clas_list").DataTable();
	
	/*
	 Start : Intializaton part
	 */

	$(".assign_to").select2();

	$('#reservation').daterangepicker();


	/*
	 End : Intializaton part
	 */


	/*
	 Start : Insert exam details with validation
	 */
	$("#create_clas").click(function(){

		var clas_name=$("#clas_name").val();
		var clas_description=$("#clas_description").val();
		var clas_teacher=$("#clas_teacher").val();
		

		if(clas_name=="" || clas_description == "" || clas_teacher == "" ){
			if(clas_name==""){
				$(".clas_name").addClass("has-error");
			}
			if(clas_description==""){
				$(".clas_description").addClass("has-error");
			}
			if(clas_teacher== "0"){
				$(".clas_teacher").addClass("has-error");
			}
			
		}
		
		else{

		 dataString='name='+clas_name+'&teacher='+clas_teacher+'&description='+clas_description;
			$.ajax({

				type: "post",
				url:"<?php echo base_url(); ?>clas/insert/ ",
				data:dataString  ,
				success: function(data){
					
					window.location.href="<?php echo base_url();?>clas/";
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});

		}

	});

		$('#clas_name').focusout( function(){

			var clas_name=$("#clas_name").val();
			if(clas_name!=''){
				$(".clas_name").removeClass("has-error");
				$(".clas_name").addClass("has-success");
			}else{
				$(".clas_name").addClass("has-error");
			}
		});
			$('#clas_teacher').focusout( function(){

				var age=$("#clas_teacher").val();
				if(clas_teacher !='0'){
					$(".clas_teacher").removeClass("has-error");
					$(".clas_teacher").addClass("has-success");
				}else{
					$(".clas_teacher").addClass("has-error");
				}
			});
				$('#clas_description').focusout( function(){

					var clas_description=$("#clas_description").val();
					if(clas_description !=''){
						$(".clas_description").removeClass("has-error");
						$(".clas_description").addClass("has-success");
					}else{
						$(".clas_description").addClass("has-error");
					}
				});


						

								/*
								 End : Insert exam details with validation
								 */


});
			</script>
<script>

$(function() {
	
	$(document).on('click', '.save', function(){

		var clas_name1=$("#clas_name1").val();
		var clas_description1=$("#clas_description1").val();
		var clas_teacher1=$("#clas_teacher1").val();
		var rev_id1 = $('#rev_id1').val();
		

		if(clas_name1=="" || clas_description1 == "" || clas_teacher1 == "" ){
			if(clas_name1==""){
				$(".clas_name1").addClass("has-error");
			}
			if(clas_description1==""){
				$(".clas_description1").addClass("has-error");
			}
			if(clas_teacher1== ""){
				$(".clas_teacher1").addClass("has-error");
			}
		}
		else{

			 dataString='name='+clas_name1+'&teacher='+clas_teacher1+'&description='+clas_description1+'&rev_id='+rev_id1;
			 			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>clas/update/",
				data:dataString  ,
				success: function(data){
						
					alert("Data Updated");
					window.location.reload();
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}
	});
		$('#clas_list').on('click', '.edit', function(){

			var id = $(this).attr('id');
			 
			var dataString='id='+id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>clas/edit/",
				data:dataString  ,
				timeout: 5000,
				success: function(data){
					$("#rev").html(data);
	     
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
					
		});
			$('#clas_list').on('click', '.delete', function(){
				event.preventDefault();
				var r = confirm("Are you sure to delete this user?");
				if (r == true){
					$(this).closest('tr').hide();
					var user_id=$(this).attr('id');

					var dataString='id='+user_id;
					$.ajax({
						type: "post",
						url:"<?php echo base_url(); ?>clas/delete/",
						data:dataString  ,
						success: function(data){

							alert("Sucessfully Deleted");
						},
						error: function(jqXHR, textStatus) {
							alert( "Request failed: " + jqXHR );
						}
					});
				}

			});
});
</script>
	    

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Class <small>Smart School</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Add Class</li>
		</ol>
	</section>

	<!-- Main content -->
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Class</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<div class="form-group clas_name">
								<label for="student_name">Class name :</label> <input
									type="text" class="form-control " id="clas_name"
									name="inputName" placeholder="Enter Class">
							</div>


							<div class="form-group clas_teacher">
								<label>Select Class Teacher</label> <select
									class="form-control select2 teacher" id="clas_teacher"
									style="width: 100%;">
									 <option value="0" >Select Teacher</option>
                <?php foreach($teacher_list as $row):?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                <?php endforeach;?>
                </select>
							</div>
							<!-- textarea -->
							<div class="form-group clas_description">
								<label>Description</label>
								<textarea class="form-control" rows="3"
									placeholder="Enter Discription" id="clas_description"></textarea>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="create_clas" class="btn btn-success">Add
								Class</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header"><h2>Class Lists</h2></div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="clas_list" class="table table-bordered table-hover css-serial">
							<thead>

								<tr>
								<th>S.No</th>
									<th>Class</th>
									<th>Class Teacher</th>
									<th>Description</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
                <?php foreach($clas_list as $selected): ?>
                <tr>
                <td></td>
									<td><?php echo $selected->c_name; ?></td>
									<td><?php echo $selected->name; ?></td>
									<td><?php echo $selected->descriptin; ?></td>
									<td align="center"><a id="<?php echo $selected->id;?>"
										class="btn btn-info btn-sm edit" data-toggle="modal"
										data-target="#myModal"> <i class="glyphicon glyphicon-edit icon-white"></i>Edit</i>
									</a> &nbsp; <a id="<?php echo $selected->id;?>"
										class="btn btn-danger btn-sm delete"><i class="glyphicon glyphicon-trash icon-white"></i>Delete</i> </a></td>

								</tr>
                <?php endforeach; ?> 
                 </tbody>
							
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Student Detail</h4>
			</div>
			<form method="post">


				<div class="reviews-form-box" id="rev"></div>



			</form>
		</div>
	</div>
</div>
<!-- ./wrapper -->


