<script type="text/javascript">
$(document).ready(function() {

	$('#exam_table').DataTable();
 	

$("#exam_table").on('click', '.exam_edit', function(){
		

		var exam_id=$(this).attr('id');
		var dataString="id="+exam_id;
		

		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>exam/exam_edit/",
			data:dataString  ,
			success: function(data){
			
				$("#exam_result").html(data);
				$(".edit_assign_to").select2();
				$('#edit_reservation').daterangepicker();

				$('#edit_exam_name').focusout( function(){

					var edit_exam_name=$("#edit_exam_name").val();
					if(edit_exam_name !=''){
				    $(".edit_exam_name").removeClass("has-error");
				    $(".edit_exam_name").addClass("has-success");
					}else{
					$(".edit_exam_name").addClass("has-error");
					}
				});

				$('#edit_duration').focusout( function(){

					var edit_duration=$("#edit_duration").val();
					if(edit_duration !=''){
				    $(".edit_duration").removeClass("has-error");
				    $(".edit_duration").addClass("has-success");
					}else{
					$(".edit_duration").addClass("has-error");
					}
				});

				$('#edit_total_mark').focusout( function(){

					var edit_total_mark=$("#edit_total_mark").val();
					if(edit_total_mark !=''){
				    $(".edit_total_mark").removeClass("has-error");
				    $(".edit_total_mark").addClass("has-success");
					}else{
					$(".edit_total_mark").addClass("has-error");
					}
				});

				$('#edit_pass_mark').focusout( function(){

					var edit_pass_mark=$("#edit_pass_mark").val();
					if(edit_pass_mark !=''){
				    $(".edit_pass_mark").removeClass("has-error");
				    $(".edit_pass_mark").addClass("has-success");
					}else{
					$(".edit_pass_mark").addClass("has-error");
					}
				});

				$('.edit_cls_val').focusout( function(){

					var edit_clas=$("#edit_clas").val();
					if(edit_clas !=null){
				    $(".edit_cls_val").removeClass("has-error");
				    $(".edit_cls_val").addClass("has-success");
					}else{
					$(".edit_cls_val").addClass("has-error");
					}
				});
								
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + jqXHR );
		    }
			});
	
	});


$(document).on('click', '#edit_exam', function(){
	
	var edit_exam_name=$("#edit_exam_name").val();
    var edit_duration=$("#edit_duration").val();
    var edit_date=$("#edit_reservation").val();
    var edit_clas=$("#edit_clas").val();
    var edit_pass_mark=$("#edit_pass_mark").val();
    var edit_total_mark=$("#edit_total_mark").val();
    var edit_description=$("#edit_description").val();
    var exam_id=$("#edit_exam_id").val();
	if(edit_exam_name=="" || edit_duration == "" || edit_total_mark == "" || edit_pass_mark=="" || edit_clas ==null){
		if(edit_exam_name==""){
		$(".edit_exam_name").addClass("has-error");
		}

		if(edit_duration == ""){
			$(".edit_duration").addClass("has-error");
		}
		if(edit_total_mark == ""){
			$(".edit_total_mark").addClass("has-error");
		}
		if(edit_pass_mark == ""){
			$(".edit_pass_mark").addClass("has-error");
		}
		if(edit_clas ==null){
			$(".edit_cls_val").addClass("has-error");
		}
		
	}else{

		if(edit_pass_mark < edit_total_mark){
		var dataString='edit_exam_name='+edit_exam_name+'&edit_date='+edit_date+'&edit_duration='+edit_duration+'&edit_clas='+edit_clas+'&edit_description='+edit_description+'&edit_total_mark='+edit_total_mark+'&edit_pass_mark='+edit_pass_mark+'&exam_id='+exam_id;
        
		
		$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>exam/exam_update/ ",
		data:dataString  ,
		success: function(data){
			location.reload(true);
		},
	    error: function(jqXHR, textStatus) {
	        alert( "Request failed: " + jqXHR );
	    }
		});
		}else{
			 alert("Please check your passmark ");
		}
	  
	}


	
});
	
$('#exam_table').on('click', '.exam_delete', function(){
		event.preventDefault();
		var r = confirm("Are you sure want to delete this exam ?");
		if (r == true){
			 $(this).closest('tr').hide();
	var exam_id=$(this).attr('id');

	var dataString='exam_id='+exam_id;
	$.ajax({
	type: "post",
	url:"<?php echo base_url(); ?>/exam/exam_delete/",
	data:dataString  ,
	success: function(data){
		
		alert("Sucessfully Deleted");
	},
	error: function(jqXHR, textStatus) {
	    alert( "Request failed: " + jqXHR );
	}
	});
		}

	});

});
</script>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Exam List <small>Smart School</small>


		</h1>

	</section>
	<div class="row">
		<div class="col-md-4"></div>
		<!-- <div class="col-md-6"><button class="btn btn-success create_exam">Add Question</button> 
	</div> -->
	</div>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Exam Details</h3>



				<div class="box-tools pull-right">

					<button type="button" class="btn btn-box-tool"
						data-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"
						data-toggle="tooltip" title="Remove">
						<i class="fa fa-times"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped"  id="exam_table">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Exam name</th>
							<th>Class</th>
							<th>Duration</th>
							<th>Total Mark</th>
							<th>Pass mark</th>
							<th width="350px">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php
							$sno = 0;
							foreach ( $exam_list as $row ) {
								$sno ++;
								?>
						<tr>
							<td><?php echo $sno;?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $row['class_name'];?></td>
							<td><?php echo $row['duration'];?> Mins</td>
							<td><?php echo $row['total_mark'];?></td>
							<td><?php echo $row['pass_mark'];?></td>
							<td ><a id="<?php echo $row['id'];?>"
								class="btn btn-info btn-sm exam_edit" data-toggle="modal"
								data-target="#exam_modal"> <i
									class="glyphicon glyphicon-edit icon-white"></i> Edit Exam
							</a> &nbsp; <a id="#" href="<?php echo base_url();?>exam/questions/<?php echo $row['id'];?>"
								class="btn btn-success btn-sm "><i
									class="glyphicon glyphicon-eye-open icon-white"></i> View questions </a> 
									&nbsp;
									<a id="<?php echo $row['id'];?>"
								class="btn btn-danger btn-sm exam_delete"><i
									class="glyphicon glyphicon-trash icon-white"></i> Delete </a></td>

						</tr>
						<?php }?>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->

			<!-- /.box-footer-->
		</div>
	</section>
</div>


<!--  -->

<div class="modal fade" id="exam_modal" tabindex="-1" role="dialog"
	aria-labelledby="exam_modal">
	<div class="modal-dialog" role="document"
		style="overflow-y: scroll; width: 700px; max-height: 85%; margin-top: 40px; margin-bottom: 50px;">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->
			<div class="modal-header">
				<b>Edit Exam</b>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<div id="exam_result"></div>
				<!-- Nested Container Ends -->
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>
