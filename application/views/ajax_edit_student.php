
<!-- Main content -->

<div class="modal-body">
<?php foreach($result as $selected): ?>
<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Student</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<input type="hidden" id="rev_id"
								value="<?php echo $selected->id;?>">
							<div class="form-group student_name">
								<label for="student_name">Student name :</label> <input
									type="text" class="form-control " id="student_name"
									name="inputName" value="<?php echo $selected->name;?>"
									placeholder="Enter Full Name">
							</div>
							<div class="form-group age">
								<label for="age">Age :</label> <input type="text"
									class="form-control " id="age" name="inputEmail"
									value="<?php echo $selected->age;?>"
									placeholder="Enter Teacher's Email">
							</div>

							<div class="form-group clas" id="change1">
								<label>Select Class</label> <select
									class="form-control select2 teacher" id="clas"
									style="width: 100%;">
									<option selected="selected"
										value="<?php echo $selected->c_id; ?>"><?php echo $selected->c_name; ?></option>
									
						                <?php foreach($class_list as $row):?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option> 
                <?php endforeach;?>
                </select>
							</div>
			

						<div class="box-footer">


							<button type="button" id="create_exam"
								class="btn btn-success save">Update Student</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
</div>
<?php endforeach; ?>
