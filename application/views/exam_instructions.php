<div class="row">
<div class="col-md-4">TOTAL QUESTION :<b><?php echo $exam_instructions->question_count;?></b></div>
<div class="col-md-4">TOTAL DURATION :<b> <?php echo $exam_instructions->duration;?> mins.</b></div>


</div>
<br>
<div class="row">
<div class="col-md-10">

<b>Please read the following instructions very carefully:</b><br>
<ul>
<li>
You have <?php echo $exam_instructions->duration;?> minutes to complete the test.</li>
<li>
The test contains a total of <?php echo $exam_instructions->question_count;?> questions.</li>
<li>
Do not refresh the page while writting exam.</li>
<li>
Do not click the button “Submit test” before completing the test. A test once submitted cannot be resumed.
</li>
</ul>
</div>
<input type="hidden" id="exam_id" value="<?php echo $exam_instructions->id;?>">

</div>