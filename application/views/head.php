<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Smart School</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/daterangepicker/daterangepicker.css">
<!-- bootstrap data tables -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/datatables/dataTables.bootstrap.css">
<!-- Editor -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/pace/pace.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url();?>main_assets/plugins/iCheck/all.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>login" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Shool</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Smart</b>School</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="http://p7cdn4static.sharpschool.com/UserFiles/Servers/Server_333558/Image/Back-to-School.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="http://p7cdn4static.sharpschool.com/UserFiles/Servers/Server_333558/Image/Back-to-School.jpg" class="img-circle" alt="User Image">

                <p>
                 <?php echo $_SESSION['username'];?>
                  <small><?php if($_SESSION['role'] ==1){ echo "Teacher"; }else if ($_SESSION['role'] ==2){ echo "Student"; }?></small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url();?>login/profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url();?>login/do_logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="http://p7cdn4static.sharpschool.com/UserFiles/Servers/Server_333558/Image/Back-to-School.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['username'];?></p>
          
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu</li>
        <?php if($_SESSION['role'] ==1){ ?>
        <li class="treeview <?php if($this->uri->segment(1) == 'login'){ echo "active";}?>">
          <a href="<?php echo base_url();?>login">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              
            </span>
          </a>
         
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'Addteacher'){ echo "active";}?>">
          <a href="#">
            <i class="fa  fa-female"></i>
            <span>Teacher</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->uri->segment(1) == 'Addteacher' && $this->uri->segment(2) != 'getlist'){ echo "active";}?>"><a href="<?php echo base_url();?>Addteacher"><i class="fa fa-circle-o"></i> Add New Teacher</a></li>
            <li class="<?php if($this->uri->segment(2) == 'getlist'){ echo "active";}?>"><a href="<?php echo base_url();?>Addteacher/getlist"> <i class="fa fa-circle-o"></i> Teachers Lists</a></li>
              </ul>
        </li>
      
       <li class="treeview <?php if($this->uri->segment(1) == 'Student'){ echo "active";}?>">
          <a href="#">
            <i class="fa fa-child"></i>
            <span>Student</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->uri->segment(1) == 'Student' && $this->uri->segment(2) != 'getlist'){ echo "active";}?>"><a href="<?php echo base_url();?>Student"><i class="fa fa-circle-o"></i> Add New Student</a></li>
            <li class="<?php if($this->uri->segment(2) == 'getlist'){ echo "active";}?>"><a href="<?php echo base_url();?>Student/getlist"><i class="fa fa-circle-o"></i> Students Lists</a></li>
              </ul>
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'clas'){ echo "active";}?>">
          <a href="<?php echo base_url();?>clas">
            <i class="fa  fa-table"></i>
            <span>Class</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span> 
          </a>
        <!--   <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul> -->
        </li>
      <!--  <li class="treeview">
          <a href="#">
            <i class="fa  fa-user"></i>
            <span>Parent</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul> 
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Lesson's</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <!--  <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul> 
        </li>-->
        <li class="treeview <?php if($this->uri->segment(1) == 'Exam'){ echo "active";}?>">
          <a href="#">
            <i class="fa fa-graduation-cap"></i>
            <span>Exams</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($this->uri->segment(1) == 'Exam' &&  $this->uri->segment(2) != 'exam_list'){ echo "active";}?>"><a href="<?php echo base_url();?>Exam"><i class="fa fa-circle-o text-green"></i>Create Exam</a></li>
            <li class="<?php if($this->uri->segment(2) == 'exam_list'){ echo "active";}?>"><a href="<?php echo base_url();?>Exam/exam_list"><i class="fa fa-circle-o text-aqua"></i>View Exam list</a></li>
            
           
          </ul>
        </li>
        <?php } else if ($_SESSION["role"]==2){?>
        <li class="treeview <?php if($this->uri->segment(2) == 'student_exam'){ echo "active";}?>">
          <a href="<?php echo base_url();?>student_exam">
            <i class="fa fa-graduation-cap"></i>
            <span>Exams </span>
            <span class="pull-right-container">
            
            </span>
          </a>
         
        </li>
        
         <li class="treeview <?php if($this->uri->segment(1) == 'student_exam' && ( $this->uri->segment(2) == 'exam_list' ||  $this->uri->segment(2) == 'exam_result')){ echo "active";}?>">
          <a href="<?php echo base_url();?>student_exam/exam_list">
            <i class="fa  fa-table"></i>
            <span>Completed Exams</span>
            <span class="pull-right-container">
             
            </span> 
          </a>
     
        </li>
        <?php }?>
        
</ul>
    </section>
    <!-- /.sidebar -->
  </aside>