<script type="text/javascript">
$(function() {
	/*
	 Start : Intializaton part
	 */

	$(".assign_to").select2();

	$('#reservation').daterangepicker();


	/*
	 End : Intializaton part
	 */


	/*
	 Start : Insert student details with validation
	 */
	$("#create_student").click(function(){

		var student_name=$("#student_name").val();
		var age=$("#age").val();
		var username=$("#username").val();
		var pass=$("#pass").val();
		var clas=$("#clas").val();
		var cpass=$("#cpass").val();
		var section=$("#section").val();

		if(student_name=="" || pass == "" || age == "" || username == "" || clas == "" ){
			if(student_name==""){
				$(".student_name").addClass("has-error");
			}
			if(age==""){
				$(".age").addClass("has-error");
			}
			if(username==""){
				$(".username").addClass("has-error");
			}
			if(pass == ""){
				$(".pass").addClass("has-error");
			}
			if(clas == "0"){
				$(".clas").addClass("has-error");
			}
			if(cpass == ""){
				$(".cpass").addClass("has-error");
			}
			
		}
		else if (pass != cpass) {  $('.duration').addClass('has-error');
		$('.cpass').addClass('has-error');}
		
		else{

		 dataString='name='+student_name+'&age='+age+'&uname='+username+'&password='+pass+'&class='+clas+'&cpass='+cpass;

			$.ajax({

				type: "post",
				url:"<?php echo base_url(); ?>student/insert/ ",
				data:dataString  ,
				success: function(data){
					
					window.location.href="<?php echo base_url();?>student/getlist/";
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});

		}

	});

		$('#student_name').focusout( function(){

			var student_name=$("#student_name").val();
			if(student_name !=''){
				$(".student_name").removeClass("has-error");
				$(".student_name").addClass("has-success");
			}else{
				$(".student_name").addClass("has-error");
			}
		});
			$('#age').focusout( function(){

				var age=$("#age").val();
				if(age !=''){
					$(".age").removeClass("has-error");
					$(".age").addClass("has-success");
				}else{
					$(".age").addClass("has-error");
				}
			});
				$('#username').focusout( function(){

					var username=$("#username").val();
					if(username !=''){
						$(".username").removeClass("has-error");
						$(".username").addClass("has-success");
					}else{
						$(".username").addClass("has-error");
					}
				});

					$('#pass').focusout( function(){

						var pass=$("#pass").val();
						if(pass !=''){
							$(".pass").removeClass("has-error");
							$(".pass").addClass("has-success");
						}else{
							$(".pass").addClass("has-error");
						}
					});

						$('#clas').focusout( function(){

							var clas=$("#clas").val();
							if(clas !='0'){
								$(".clas").removeClass("has-error");
								$(".clas").addClass("has-success");
							}else{
								$(".clas").addClass("has-error");
							}
						});
							$('#cpass').focusout( function(){

								var cpass=$("#cpass").val();
								if(cpass !=''){
									$(".cpass").removeClass("has-error");
									$(".cpass").addClass("has-success");
								}else{
									$(".cpass").addClass("has-error");
								}
							});
							
							
								/*
								 End : Insert student details with validation
								 */


});
			</script>



<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Student <small>Smart School</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Add New Student</li>
		</ol>
	</section>

	<!-- Main content -->
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Student</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<div class="form-group student_name">
								<label for="student_name">Student name :</label> <input
									type="text" class="form-control " id="student_name"
									name="inputName" placeholder="Enter Full Name">
							</div>
							<div class="form-group age">
								<label for="age">Age :</label> <input type="number"
									class="form-control " id="age" name="inputEmail"
									placeholder="Enter Age">
							</div>
							<div class="form-group username">
								<label for="username">User name :</label> <input type="text"
									class="form-control " id="username" name="inputUName"
									placeholder="Enter User Name">
							</div>
							<div class="form-group pass">
								<label for="pass">Password :</label> <input type="password"
									class="form-control " id="pass" name="inputPassword"
									placeholder="Enter Password">
							</div>
							<div class="form-group cpass">
								<label for="cpass">Confirm Password :</label> <input type="password"
									class="form-control " id="cpass" name="inputPassword"
									placeholder="Enter Confirm Password">
							</div>

						
							<div class="form-group clas" id="change1">
								<label>Select Class</label> <select
									class="form-control select2 teacher" id="clas"
									style="width: 100%;">
									 <option value="0" >Select Class</option>
                <?php foreach($student as $row):?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                <?php endforeach;?>
                </select>
							</div>
							
					
						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="create_student" class="btn btn-success">Add
								Student</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>

