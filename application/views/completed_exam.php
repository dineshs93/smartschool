






<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Student Completed Exam List <small>Smart School</small>


		</h1>

	</section>
	<div class="row">
		<div class="col-md-4"></div>
		<!-- <div class="col-md-6"><button class="btn btn-success create_exam">Add Question</button> 
	</div> -->
	</div>

	<section class="content">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Exam List</h3>
			</div>
			
			<div class="box-body">
				<table class="table table-striped"  id="exam_list">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Exam name</th>
							<th>Total Mark</th>
							<th>Obtained Mark</th>
							<th>Pass mark</th>
							<th>Result Status</th>
							<th width="350px">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php $sno = 0;
							foreach ( $exam_list as $row ) {
								$sno ++;
								?>
						<tr>
							<td><?php echo $sno;?></td>
							<td><?php echo $row['exam_name'];?></td>
							<td><?php echo $row['total_mark'];?></td>
							<td><?php echo $row['marks_obtained'];?></td>
							<td><?php echo $row['pass_mark'];?></td>
							<td><?php if($row['pass_status'] == 1){?><span class="label label-success glyphicon glyphicon-thumbs-up"> PASS</span><?php }else{ ?><span class="label label-danger glyphicon glyphicon-thumbs-down"> FAIL</span><?php }?></td>	
						<td><a id="" href="<?php echo base_url();?>student_exam/exam_result/<?php echo $row['exam_id'];?>"
								class="btn btn-info btn-sm " 
								> <i
									class="glyphicon glyphicon-eye-open icon-white"></i> View Result
							</a></td>

						</tr>
						<?php }?>
					</tbody>
				</table>

			</div>
			<!-- /.box-body -->
			</div>
			</section>
			
			 
			</div>
			
			
			
			
		