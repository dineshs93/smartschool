<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Smart School</title>

        <!-- CSS -->
        
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css">
		    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!-- Favicon and touch icons -->
        
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">



<script type="text/javascript">
  

$(document).ready(function(){
	
	$("#error_alert").hide();
	 

	
	$("#login").click(function(){
        var role= $('#role').val();
		var username=$('#username').val();
		var password=$('#password').val();

		if(username == '' || password== ''){
	    $('.error').html("Please enter all fields.");
	    
	    $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#error_alert").slideUp(500);
             });  
	   
		}else{
			
			var dataString='username='+username+'&password='+password+'&role='+role;
			
			$.ajax({
			type: "POST",
			url:"<?php echo base_url(); ?>login/login_check",
			data:dataString  ,
			success: function(data){

				if(data == "failure"){
					$('.error').html("Invalid username and password");
				    
				    $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
			            $("#error_alert").slideUp(500);
			             }); 

				}else{
					location.reload(true);
				}
			},
		    error: function(jqXHR, textStatus) {
		        alert( "Request failed: " + textStatus );
		    }
			});

		}
		

	});
});

</script>


    </head>

    <body >

        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to Smart School</h3>
                            		<p>Enter your username and password to log on:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                  <div class="alert alert-danger" id="error_alert">
			                  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong class="error"> </strong>
</div>
<div class="form-group">
			                    		<label class="sr-only" for="form-role">Select Role</label>
			                        	<select class="form-control" id="role"><option value="1">Teacher</option><option value="2">Student</option> </select>
			                        </div>

			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="username" id="username" placeholder="Username..." class="form-username form-control" >
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" id="password" placeholder="Password..." class="form-password form-control" >
			                        </div>
			                        <button  id="login" class="btn btn-success">Log In!</button>
			                    
		                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>


        <!-- Javascript -->
        <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
