
<!-- Main content -->

<div class="modal-body">

          <?php foreach($result as $selected): ?>
<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Teacher</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<div class="form-group teacher_name">
								<input type="hidden" id="rev_id"
									value="<?php echo $selected->id;?>"> <label for="teacher_name">Teacher
									name :</label> <input type="text" class="form-control "
									id="teacher_name" name="inputName"
									value="<?php echo $selected->name;?>">
							</div>
							<div class="form-group mail">
								<label for="mail">Email :</label> <input type="email"
									class="form-control " id="mail" name="inputEmail"
									value="<?php echo $selected->email;?>"
									placeholder="Enter Teacher's Email">
							</div>
							<div class="form-group username">
								<label for="username">User name :</label> <input type="text"
									class="form-control " id="username" name="inputUName"
									value="<?php echo $selected->username;?>"
									placeholder="Enter User Name">
							</div>

							<div class="form-group mobile">
								<label>Mobile:</label>

								<div class="input-group">
									<input type="number" class="form-control" name="inputMobile"
										value="<?php echo $selected->mobile_Number;?>" id="mobile"> <span
										class="input-group-addon">PH</span>
								</div>
							</div>




						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="create_teacher"
								class="btn btn-success save">Update Teacher</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
</div>
<?php endforeach; ?>
