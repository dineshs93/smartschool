
<script>

$(function() {
	$("#student_list").DataTable();
	/*
	 End : update save student details with validation
	 */
	$(document).on('click', '.save', function(){

		var student_name=$("#student_name").val();
		var age=$("#age").val();
		var username=$("#username").val();
		var pass=$("#pass").val();
		var clas=$("#clas").val();
		var cpass=$("#cpass").val();
		var section=$("#section").val();
		var rev_id = $('#rev_id').val();

		
		if(student_name=="" || pass == "" || age == "" || username == "" || clas == "" || section == ""){
			if(student_name==""){
				$(".student_name").addClass("has-error");
			}
			if(age==""){
				$(".age").addClass("has-error");
			}
			if(username==""){
				$(".username").addClass("has-error");
			}
			if(clas == ""){
				$(".clas").addClass("has-error");
			}
			if(section == ""){
				$(".section").addClass("has-error");
			}
			
		}
		else if (pass != cpass) {  $('.duration').addClass('has-error');
		$('.cpass').addClass('has-error');}
		else{

			 dataString='name='+student_name+'&age='+age+'&uname='+username+'&clas='+clas+'&section='+section+'&rev_id='+rev_id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>/Student/update/",
				data:dataString  ,
				success: function(data){
					alert("Data Updated");
					window.location.reload();
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}
	});
	/*
	 End : edit student details with validation
	 */
		$('#student_list').on('click', '.edit', function(){

			var id = $(this).attr('id');
			 
			var dataString='id='+id;
				
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>/Student/edit/",
				data:dataString  ,
				timeout: 5000,
				success: function(data){
						
					$("#rev").html(data);
	     
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
					
		});
		/*
		 End :delete student details with validation
		 */
			$('#student_list').on('click', '.delete', function(){
				event.preventDefault();
				var r = confirm("Are you sure to delete this user?");
				if (r == true){
					$(this).closest('tr').hide();
					var user_id=$(this).attr('id');

					var dataString='id='+user_id;
					$.ajax({
						type: "post",
						url:"<?php echo base_url(); ?>/Student/delete/",
						data:dataString  ,
						success: function(data){

							alert("Sucessfully Deleted");
						},
						error: function(jqXHR, textStatus) {
							alert( "Request failed: " + jqXHR );
						}
					});
				}

			});

		$('#student_name').focusout( function(){

			var student_name=$("#student_name").val();
			if(student_name !=''){
				$(".student_name").removeClass("has-error");
				$(".student_name").addClass("has-success");
			}else{
				$(".student_name").addClass("has-error");
			}
		});
			$('#age').focusout( function(){

				var age=$("#age").val();
				if(age !=''){
					$(".age").removeClass("has-error");
					$(".age").addClass("has-success");
				}else{
					$(".age").addClass("has-error");
				}
			});
				$('#username').focusout( function(){

					var username=$("#username").val();
					if(username !=''){
						$(".username").removeClass("has-error");
						$(".username").addClass("has-success");
					}else{
						$(".username").addClass("has-error");
					}
				});

					$('#pass').focusout( function(){

						var pass=$("#pass").val();
						if(pass !=''){
							$(".pass").removeClass("has-error");
							$(".pass").addClass("has-success");
						}else{
							$(".pass").addClass("has-error");
						}
					});

						$('#clas').focusout( function(){

							var clas=$("#clas").val();
							if(clas !=''){
								$(".clas").removeClass("has-error");
								$(".clas").addClass("has-success");
							}else{
								$(".clas").addClass("has-error");
							}
						});
							$('#cpass').focusout( function(){

								var cpass=$("#cpass").val();
								if(cpass !=''){
									$(".cpass").removeClass("has-error");
									$(".cpass").addClass("has-success");
								}else{
									$(".cpass").addClass("has-error");
								}
							});
							$('#section').focusout( function(){

								var section=$("#section").val();
								if(section !=''){
									$(".section").removeClass("has-error");
									$(".section").addClass("has-success");
								}else{
									$(".section").addClass("has-error");
								}
							});

								/*
								 End : Insert exam details with validation
								 */


});
			</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Students Lists</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Student Lists</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header"></div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="student_list" class="table table-bordered table-hover css-serial">
							<thead>

								<tr>
								<th>S.No</th>
									<th>Student Name</th>
									<th>Age</th>
									<th>class</th>
									<th>Section</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
			<?php foreach($teacher_list as $selected): 
			$sno=0;
			$sno++;
			?>
                <tr>
                <td><?php echo $sno;?></td>
									<td><?php echo $selected->name; ?></td>
									<td><?php echo $selected->age; ?></td>
									<td><?php echo $selected->class; ?></td>
									<td><?php echo $selected->section; ?></td>
									<td align="center"><a id="<?php echo $selected->id;?>"
										class="btn btn-info btn-sm edit" data-toggle="modal"
										data-target="#myModal"><i class="glyphicon glyphicon-edit icon-white"></i>Edit
									</a> &nbsp; <a id="<?php echo $selected->id;?>"
										class="btn btn-danger btn-sm delete"><i
													class="glyphicon glyphicon-trash icon-white"></i>Delete</a></td>

								</tr>
                <?php endforeach; ?> 
                 </tbody>
							
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->


			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Student Detail</h4>
			</div>
			<form method="post">


				<div class="reviews-form-box" id="rev"></div>



			</form>
		</div>
	</div>
</div>
<!-- ./wrapper -->

