<script type="text/javascript">
$(document).ajaxStart(function() { Pace.restart(); });
$(function() {

	/*
	Start : Intializaton part
	*/

	$(".assign_to").select2();
	
	$('#reservation').daterangepicker();


	/*
	End : Intializaton part
	*/

	
	/*
	Start : Insert exam details with validation 
	*/
	
$("#create_exam").click(function(){

	var exam_name=$("#exam_name").val();
    var duration=$("#duration").val();
    var date=$("#reservation").val();
    var clas=$("#clas").val();
    var pass_mark=$("#pass_mark").val();
    var total_mark=$("#total_mark").val();
    var description=$("#description").val();
    
	if(exam_name=="" || duration == "" || total_mark == "" || pass_mark=="" || clas == null){
		if(exam_name==""){
		$(".exam_name").addClass("has-error");
		}

		if(duration == ""){
			$(".duration").addClass("has-error");
		}
		if(total_mark == ""){
			$(".total_mark").addClass("has-error");
		}
		if(pass_mark == ""){
			$(".pass_mark").addClass("has-error");
		}

		if(clas ==null){
			$(".cls_val").addClass("has-error");
		}
		
	}else{

		if(pass_mark < total_mark){
			
		var dataString='exam_name='+exam_name+'&date='+date+'&duration='+duration+'&clas='+clas+'&description='+description+'&total_mark='+total_mark+'&pass_mark='+pass_mark;
        
		
		$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>exam/exam_insert/ ",
		data:dataString  ,
		success: function(data){
			window.location.href="<?php echo base_url();?>exam/questions/"+data;
		},
	    error: function(jqXHR, textStatus) {
	        alert( "Request failed: " + jqXHR );
	    }
		});
		}else{
        alert("Please check your passmark ");
		}
	  
	}

});

$('#exam_name').focusout( function(){

	var exam_name=$("#exam_name").val();
	if(exam_name !=''){
    $(".exam_name").removeClass("has-error");
    $(".exam_name").addClass("has-success");
	}else{
	$(".exam_name").addClass("has-error");
	}
});

$('#duration').focusout( function(){

	var duration=$("#duration").val();
	if(duration !=''){
    $(".duration").removeClass("has-error");
    $(".duration").addClass("has-success");
	}else{
	$(".duration").addClass("has-error");
	}
});

$('#total_mark').focusout( function(){

	var total_mark=$("#total_mark").val();
	if(total_mark !=''){
    $(".total_mark").removeClass("has-error");
    $(".total_mark").addClass("has-success");
	}else{
	$(".total_mark").addClass("has-error");
	}
});

$('#pass_mark').focusout( function(){

	var pass_mark=$("#pass_mark").val();
	if(pass_mark !=''){
    $(".pass_mark").removeClass("has-error");
    $(".pass_mark").addClass("has-success");
	}else{
	$(".pass_mark").addClass("has-error");
	}
});

$('.cls_val').focusout( function(){

	var clas=$("#clas").val();
	if(clas !=null){
    $(".cls_val").removeClass("has-error");
    $(".cls_val").addClass("has-success");
	}else{
	$(".cls_val").addClass("has-error");
	}
});


/*
End : Insert exam details with validation 
*/



	
	
});
</script>





<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Exam <small>Smart School</small>
		</h1>

	</section>

	<!-- Main content -->
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Create Exam</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form>
						<div class="box-body">
							<div class="form-group exam_name">
								<label for="examname">Exam name :</label> <input type="text"
									class="form-control " id="exam_name"
									placeholder="Enter exam title">
							</div>


							<div class="form-group">
								<label>Date Between:</label>

								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" class="form-control pull-right"
										id="reservation">
								</div>
								<!-- /.input group -->
							</div>

							<div class="form-group duration">
								<label>Duration:</label>

								<div class="input-group">
									<input type="number" class="form-control" id="duration"> <span
										class="input-group-addon">MINS</span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group total_mark">
										<label>Total Mark:</label>

									<input type="number" class="form-control" id="total_mark"> 
								
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pass_mark">
										<label>Pass Mark:</label>

										
									<input type="number" class="form-control" id="pass_mark"> 
								
									</div>
								</div>
							</div>
							<div class="form-group cls_val">
								<label>Class :</label> <select class="form-control assign_to"
									multiple="multiple" data-placeholder="Select Class" id="clas"
									style="width: 100%;">
									
									<?php foreach($clas as $row){?>
<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>

<?php }?>
								</select>
							</div>
							<div class="form-group">
								<label>Description :</label> <input type="text"
									class="form-control" id="description">


							</div>

						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="create_exam" class="btn btn-success" >Create
								Exam</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>

