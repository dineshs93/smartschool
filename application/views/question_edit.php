


<div class="container-fluid">


					<div class="row">
						<form>
							<input type="hidden" id="edit_question_id"
								value="<?php echo $question_edit->id;?>">
								<input type="hidden" id="edit_exam_id"
								value="<?php echo $question_edit->exam_id;?>">
							<div class="box-body">
								<div class="form-group edit_question_name">
									<label for="question">Question :</label>
									<textarea class="editor" id="editor1" name="editor1" rows="2"
										cols="30" ><?php echo utf8_decode($question_edit->question);?>
                                            
                    </textarea>
								</div>





								<div class="row">
									<div class="col-md-6">
										<div class="form-group edit_set_mark">
											<label>Set Mark:</label> <input type="number"
												class="form-control" id="edit_set_mark" value="<?php echo $question_edit->mark;?>">

										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group edit_answer_type">
											<label>Answer Type:</label> <select class="form-control "
												id="edit_answer_type">
												<option value="0" >Choose type</option>
												<option value="1" <?php if($question_edit->type ==1 ) echo "selected";?>>Text mode</option>
												<option value="2" <?php if($question_edit->type ==2 ) echo "selected";?>>Optional mode</option>
											</select>

										</div>
									</div>
								</div>

								<div class="form-group " id="edit_text_answer">
									<label>Answer :</label> <input type="text" class="form-control"
										name="edit_text_answer" id="edit_text_ans" value="<?php echo utf8_decode($question_edit->answer);?>">
								</div>




								<div class="form-group" id="edit_option_answer">
									<div class="alert alert-danger" id="edit_error_alert">
										<button type="button" class="close" data-dismiss="alert">x</button>
										<strong class="edit_error_crt_answer"> </strong>
									</div>

									<div class="alert alert-danger" id="edit_error_alert1">
										<button type="button" class="close" data-dismiss="alert">x</button>
										<strong class="edit_error_option_answer"> </strong>
									</div>
									<table class="table table-striped">
										<tr>
											<th style="width: 10px">Options</th>
											<th style="width: 30px">Coreect Answer</th>
											<th>Answer</th>

										</tr>
										<tr>
											<td>a.</td>
											<td><input type="radio" name="r3" class="edit_flat-green"
												value="optiona" <?php if($question_edit->option_ans == "optiona") echo "checked"; ?>></td>
											<td><input type="text" class="form-control" id="edit_optiona"
												name="edit_optiona" value="<?php echo $question_edit->optiona;?>"></td>

										</tr>
										<tr>
											<td>b.</td>
											<td><input type="radio" name="r3" class="edit_flat-green"
												value="optionb" <?php if($question_edit->option_ans == "optionb") echo "checked"; ?>></td>
											<td><input type="text" class="form-control" id="edit_optionb"
												name="edit_optionb" value="<?php echo $question_edit->optionb;?>"></td>

										</tr>
										<tr>
											<td>c.</td>
											<td><input type="radio" name="r3" class="edit_flat-green"
												value="optionc" <?php if($question_edit->option_ans == "optionc") echo "checked"; ?>></td>
											<td><input type="text" class="form-control" id="edit_optionc"
												name="edit_optionc" value="<?php echo $question_edit->optionc;?>"></td>

										</tr>
										<tr>
											<td>d.</td>
											<td><input type="radio" name="r3" class="edit_flat-green"
												value="optiond" <?php if($question_edit->option_ans == "optiond") echo "checked"; ?>></td>
											<td><input type="text" class="form-control" id="edit_optiond"
												name="edit_optiond" value="<?php echo $question_edit->optiond;?>"></td>

										</tr>

									</table>

								</div>



							</div>
							<!-- /.box-body -->

							<div class="modal-footer">


								<button type="button" id="edit_question"
									class="btn btn-success">Save</button>


							</div>
						</form>



					</div>
				</div>