<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Exam Result <small>Smart School</small>


		</h1>

	</section>
	<div class="row">
		<div class="col-md-4"></div>

	</div>

	<section class="content">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Exam Information</h3>
			</div>

			<div class="box-body">
				<div class="row">

					<div class="col-md-4">
						<h4>Exam Name : <?php echo $exam_details->exam_name;?></h4>
					</div>
					<div class="col-md-4">
						<h4>Date : <?php echo $exam_details->start_time;?></h4>
					</div>
					<div class="col-md-4">
						<h4>Total Mark : <?php echo $exam_details->total_mark;?></h4>
					</div>
				</div>
				<div class="row">

					<div class="col-md-4">
						<h4>Pass Mark : <?php echo $exam_details->pass_mark;?></h4>
					</div>
					<div class="col-md-4">
						<h4>Obtained Marks :<?php echo $exam_details->marks_obtained;?></h4>
					</div>
					<div class="col-md-4">
						<h4>
							Exam Result : <?php if($exam_details->pass_status == 1){?><span class="label label-success glyphicon glyphicon-thumbs-up"> PASS</span><?php }else{ ?><span class="label label-danger glyphicon glyphicon-thumbs-down"> FAIL</span><?php }?>
						</h4>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>

		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Detail Information</h3>
			</div>

			<div class="box-body">
			<table class="table table-striped"  id="exam_list">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Question</th>
							<th>Mark</th>
							<th>Your Response</th>
							<th>Correct Answer</th>
							<th>Result</th>
							
						</tr>
					</thead>
					<tbody>
					<?php 
					$sno=1;
					foreach($question_details as $row){
						
					?>
					<tr>
					<td> <?php echo $sno;?></td>
					<td><?php echo $row["question"];?></td>
					<td><?php echo $row["mark"];?></td>
					<td><?php echo $row["your_answer"];?></td>
					<td><?php echo $row["correct_answer"];?></td>
					<td><?php if($row["correct_answer"]==$row["your_answer"]){?><span class="label label-success glyphicon glyphicon-ok"> Correct</span><?php }else{?><span class="label label-danger glyphicon glyphicon-remove"> Wrong</span><?php }?></td>
					</tr>
					<?php $sno++;}?>
					</tbody>
					</table>
					
			
			
			
			</div>
			<!-- /.box-body -->
		</div>
	</section>


</div>