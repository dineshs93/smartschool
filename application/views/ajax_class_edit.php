
<!-- Main content -->

<div class="modal-body">
<?php foreach($result as $selected): ?>
<section class="content">
		<div class="row">
			<div class="col-md-1"></div>
			<!-- left column -->
			<div class="col-md-10">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Class</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form">
						<div class="box-body">
							<input type="hidden" id="rev_id1"
								value="<?php echo $selected->id;?>">
							<div class="form-group clas_name1">
								<label for="student_name">Class name :</label> <input
									type="text" class="form-control " id="clas_name1"
									name="inputName" placeholder="Enter Class" value="<?php echo $selected->c_name; ?>">
							</div>


							<div class="form-group clas_teacher1" id="change1">
								<label>Select Class Teacher</label> <select
									class="form-control select2 teacher" id="clas_teacher1"
									style="width: 100%;">
									<option selected="selected"
										value="<?php echo $selected->t_id; ?>"><?php echo $selected->name; ?></option>
									
                <?php foreach($teacher_list as $row):?>
                  <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                <?php endforeach;?>
                </select>
							</div>
							<!-- textarea -->
							<div class="form-group clas_description1">
								<label>Textarea</label>
								<textarea class="form-control" rows="3"
									placeholder="Enter Discription" id="clas_description1" ><?php echo $selected->descriptin; ?></textarea>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">


							<button type="button" id="class_list_update"
								class="btn btn-success save">Update Class</button>


						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
</div>
<?php endforeach; ?>
