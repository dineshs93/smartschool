


<div class="container-fluid">


	<div class="row">
		<form>
			<input type="hidden" id="edit_exam_id"
				value="<?php echo $exam_edit->id;?>">
			<div class="box-body">
				<div class="form-group edit_exam_name">
					<label for="examname">Exam name :</label> <input type="text"
						class="form-control " id="edit_exam_name"
						value="<?php echo $exam_edit->name;?>">
				</div>


				<div class="form-group">
					<label>Date Between:</label>

					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
									<?php
									$f_date = date_create ( $exam_edit->start_date );
									$start_date = date_format ( $f_date, "m/d/Y" );
									
									$e_date = date_create ( $exam_edit->end_date );
									$end_date = date_format ( $e_date, "m/d/Y" );
									
									?>
									<input type="text" class="form-control pull-right"
							id="edit_reservation"
							value="<?php echo $start_date;?> - <?php echo $end_date;?>">
					</div>
					<!-- /.input group -->
				</div>

				<div class="form-group edit_duration">
					<label>Duration:</label>

					<div class="input-group">
						<input type="number" class="form-control" id="edit_duration"
							value="<?php echo $exam_edit->duration;?>"> <span
							class="input-group-addon">MINS</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group edit_total_mark">
							<label>Total Mark:</label> <input type="number"
								class="form-control" id="edit_total_mark"
								value="<?php echo $exam_edit->total_mark;?>">

						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group edit_pass_mark">
							<label>Pass Mark:</label> <input type="number"
								class="form-control" id="edit_pass_mark"
								value="<?php echo $exam_edit->pass_mark;?>">

						</div>
					</div>
				</div>
				<div class="form-group edit_cls_val">
					<label>Class :</label> <select class="form-control edit_assign_to"
						multiple="multiple" data-placeholder="Select Class" id="edit_clas"
						style="width: 100%;">
<?php foreach($clas as $row){?>

<?php
	$boolean = FALSE;
	$val = $exam_edit->class;
	$clas_values = explode ( ',', $val );
	$cls_original = $row ['id'];
	foreach ( $clas_values as $cls_val ) {
		if ($cls_val == $cls_original) {
			$boolean = TRUE;
			?>
<option value="<?php echo $row['id'];?>" selected="selected"><?php echo $row['name'];?></option>
<?php }}?>
<?php
	if ($boolean != TRUE) {
		?>
<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
<?php }?>
<?php }?>

								</select>
				</div>
				<div class="form-group">
					<label>Description :</label> <input type="text"
						class="form-control" id="edit_description"
						value="<?php echo $exam_edit->description;?>">


				</div>

			</div>
			<!-- /.box-body -->

			<div class="modal-footer">


				<button type="button" id="edit_exam" class="btn btn-success">Save</button>


			</div>
		</form>



	</div>
</div>